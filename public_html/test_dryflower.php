<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ドライフラワー加工｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<script type="text/javascript" src="./ci/js/top.js"></script>


</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">

<div id="main">
<div id="dryflower">
    
 
<h2><img src="images/title-dryflower.png" alt="ドライフラワー加工"></h2>





<section>

<p class="mb10"><img src="images/dryflower/catch.jpg" alt="せっかく受け取った幸運なブーケだから、永遠のインテリアに…"></p>

<div class="txt txt_catch">
<p>結婚式後のブーケを受け取った幸運なあなたは、その後どのようにされていますか？</p>
<p>おそらく、家に持ち帰ったとしても、いつかは枯れ果ててしまう悲しい結末になってないでしょうか？<br>
  だったら、せっかく受け取った幸運なブーケを、永遠に残る素敵な思い出にしましょう。</p>
<p>当店では、<strong>ブライダルブーケを素敵なインテリアに変身させるサービス</strong>を行っております。<br>
  あなたのブーケを永遠の感動に･･･お気軽にお問い合わせ下さいませ。</p>
</div>
 
</section>

<section class="bg_wt">
<img src="images/dryflower/natural-dry.jpg" alt="ナチュラルドライ">
	
	</section>

<section class="bg_wt">

<p class="center"><img src="images/dryflower/booke.png" alt="幸運のブーケを永遠のインテリアにドライフラワー加工"></p>

<table class="tbl_type">
 	<tr>
 		<th><img src="images/dryflower/type-1.png" alt="Crystal"></th>
 		<th><img src="images/dryflower/type-2.png" alt="Set Plan"></th>
 		<th><img src="images/dryflower/type-3.png" alt="Frame"></th>
 		<th><img src="images/dryflower/type-4.png" alt="Frame"></th>
 	</tr>
 	<tr>
 		<td>生花から立体（凸らシオンフルール）に加工して、クリスタルにお入れいたします。</td>
 		<td>1つのブーケから分けて、贈り物や当日に両家へのサプライズプレゼントにもおすすめです。</td>
 		<td>生花から立体（デコラシオンフルール）に加工して、3種類から選べる額にお入れいたします。</td>
 		<td>生花から押花（プレセフルール）に加工して、3種類から選べる額にお入れいたします。</td>
 	</tr>    
</table>
 
 
</section>



<section>

<h3>クリスタルデコラシオンフルール<span>（クリスタル立体）</span></h3>


<div class="txt catch_sub">
<p>クリスタルに包まれてまるでブーケのように・・・。<br>
  どの角度からもお花の色どりや形を楽しむことができます。</p>
</div>

<p class="center"><img src="images/dryflower/pic01.jpg" alt="" /></p>

</section>




<section>

<h3>クデコラシオンフルール<span>（立体額）</span></h3>


<div class="txt catch_sub">
<p>額に入れて生け花ブーケのイメージを活かしたアレンジに・・・。<br>
台紙やフレームで様々なイメージに変わります。</p>
</div>

<p class="center"><img src="images/dryflower/pic02.jpg" alt="" /></p>

</section>




<section>

<h3>プレセフルール<span>（押花額）</span></h3>


<div class="txt catch_sub">
<p>まるで絵画のようにフレームにおさまった花びらのアート。思い出を重ねるように、花びらも一枚一枚丁寧に重ねていきます。</p>
</div>

<p class="center"><img src="images/dryflower/pic03.jpg" alt="" /></p>

</section>









 
<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->










</div><!-- dryflower -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>