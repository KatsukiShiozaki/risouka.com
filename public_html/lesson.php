<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>レッスン｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<script type="text/javascript" src="./ci/js/top.js"></script>


</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">

<div id="main">

<div id="lesson">
<h2><img src="images/title-lesson.png" alt="レッスン"></h2>



<section>
<p class="mb10"><img src="images/lesson/catch.jpg" alt="「喜」「楽」「笑」で　お花を極める、それが理創花のフラワースクールです。"></p>


  <div class="txt txt_catch">
    <p><strong>『生活の中にお花をとりいれる』</strong></p>
    <p>理創花のフラワースクールでは生花、プリザーブドフラワー、ドライフラワー、リボン等、花材の特徴を活かした表現力をお伝えします。</p>
    <p>資格取得コースや趣味のコースなど目的に合わせた豊富なコースをご用意しています。<br>
      是非一緒にお花の魅力を堪能しましょう！</p>
  </div>
</section>




<section id="soapflower">
    <h3>ソープフラワーレッスン</h3>
    
    
    <p class="icon">
    <img src="images/lesson/icon-sikaku.png" alt="資格">
    <img src="images/lesson/icon-shumi.png" alt="趣味">
    
    
    </p>
    
    
    <div class="txt">
      <p>石鹸の素材の花を使います。<br>
       趣味のコース。      </p>
    </div>
    
    
    <div class="txt summary">
    <h4>レッスン概要</h4>
    <p>香りのよいパフュームローズ、パフュームカーネーション、アーティフィシャルフラワー、ﾌﾟﾘｻﾞｰﾌﾞﾄﾞﾌﾗﾜｰ等とのコラボしたレッスンになります。<br>
      異種素材の組み合わせをたのしみながら学ぶ、贅沢なレッスンです。<br>
      趣味のｺｰｽでは、単発レッスン・予算に合わせるレッスンが可能です。<br>
      講師活動されたい方はご相談くださいませ。</p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>


<section id="orne">
    <h3>オルネフラワー協会</h3>
    
    
    <p class="icon">
    <img src="images/lesson/icon-sikaku.png" alt="資格">
    <img src="images/lesson/icon-shumi.png" alt="趣味"></p>
    
    
    <div class="txt">
      <p>金属･ﾊﾟｰﾙ装飾の普及と技術向上発展に寄与することを目的に発足した協会です。</p>
    </div>
    
    
    <div class="txt summary">
    <h4>レッスン概要</h4>
    <p>会員は、趣味として金属装飾。パール装飾を楽しみながらレッスンﾌﾟﾛ、デザイナーとして活動ができます。<br>
      オルネフラワーコース・オルネプリザーブドフラワー・クロスターアルバイテンコース等があります。<br>
      協会独自の素敵なデザインなどたのしみながら、ｱﾚﾝｼﾞ、インテリアなどに他に見ないクオリティをお試しください。<br>
    </p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>















<section id="dryflower">
    <h3>ドライフラワーレッスン</h3>
    
    
    <p class="icon">
    <img src="images/lesson/icon-sikaku.png" alt="資格">
    <img src="images/lesson/icon-shumi.png" alt="趣味"></p>
    
    
    <div class="txt"></div>
    
    
    <div class="txt summary">
    <h4>レッスン概要</h4>
    <p>生花をドライフラワーにして、額、ガラスドーム、等にアレンジします。<br>
      気にいったパーツがあればアクセサリーに変身させましょう。</p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>












<section id="holland">
    <h3>オランダスタイル</h3>
    
    
    <p class="icon">
    <img src="images/lesson/icon-sikaku.png" alt="資格">
    <img src="images/lesson/icon-shumi.png" alt="趣味"></p>
    
    
    <div class="txt"></div>
    
    
    <div class="txt summary">
    <h4>レッスン概要</h4>
    <p>材料・カラー・デザインについて判断ができるようなことまなびながら、アレンジ・ブーケ・壁デコレーション・テーブルデコレーション・コサージュ<br>
      などを楽しみます。シーズンアレンジは、四季を楽しめるので、オススメです。</p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>






<h2 id="license">各種資格取得コース </h2>






<section>
    <h3>学習フォーラムカルチャーレッスン</h3>
    <p>桂由美のオートクチュール・フルール</p>
    <p class="icon">
      <img src="images/lesson/icon-sikaku.png" alt="資格">
      <img src="images/lesson/icon-shumi.png" alt="趣味"></p>
    
    
    <div class="txt"></div>
    
    
    <div class="txt summary">
    <h4>花ビラメーキング</h4>
    <p>高品質のアーティフィシャルフラワーを使用してソラバナ（空想的花葉）と名付けられた花の造形美を楽しみます。発想がふくらみます。</p>
    <h4> ブライダルコース</h4>
    <p>花ビラメーキングコース終了した方のみが受講可能です。高品質のアーティフィシャルフラワーを使用し正統派のブーケから和装にも合わせられる個性的なﾌﾞｰｹ等<br>
      ４つのブーケから会場装花などを学びます。    </p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>













<section id="">
    <h3>オートクチュール・フルール認定講座</h3>
    
    
    <p class="icon">
    <img src="images/lesson/icon-sikaku.png" alt="資格">
    <img src="images/lesson/icon-shumi.png" alt="趣味"></p>
    
    
    <div class="txt"></div>
    
    
    <div class="txt summary">
    <h4>スタイリングコース</h4>
    <p>雑貨とアーティフィシャルフラワーとのコーディネートの中でワイヤークラフト、コラージュなどのクラフトに触れながら新たなアーティフィシャルフラワーの表情を楽しみます。全６レッスン<br>
      レッスン後は、認定キットなどが、シーズンに合わせてご紹介されます。</p>
    <h4>マクラメジュエリー認定講座</h4>
    <p>マクラメをコスチュームジュエリーの制作を通して学びます。    </p>
    </div>
    
    
    <p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
    
    
</section>














































<!--<section id="soapflower">
<h3>ソープフラワー</h3>


<p class="icon">
<img src="images/lesson/icon-sikaku.png" alt="資格">
<img src="images/lesson/icon-shumi.png" alt="趣味"></p>


<div class="txt">
  <p>サンプルテキストサンプルテキスト</p>
</div>


<div class="txt summary">
<h4>レッスン概要</h4>
<p>テキストが入ります。
サンプルテキストサンプルテキスト。</p>
</div>


<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>


</section>

-->



<!--
<section id="orne">
<h3>オルネプリザーブド</h3>


<div class="txt">
  <p>サンプルテキストサンプルテキスト</p>
</div>

<div class="txt summary">
<h4>レッスン概要</h4>
<p>テキストが入ります。
サンプルテキストサンプルテキスト。</p>
</div>

<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>


</section>
-->

<!--
<section id="holland">
<h3>オランダスタイルフラワーレッスン</h3>


<div class="txt">
  <p>サンプルテキストサンプルテキスト</p>
</div>

<div class="txt summary">
<h4>レッスン概要</h4>
<p>テキストが入ります。
サンプルテキストサンプルテキスト。</p>
</div>

<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>


</section>-->


<!--

<section id="dryflower">
<h3>ドライフラワーレッスン</h3>



<div class="txt">
  <p>サンプルテキストサンプルテキスト</p>
</div>


<div class="txt summary">

<h4>レッスン概要</h4>
<p>テキストが入ります。
サンプルテキストサンプルテキスト。</p>
</div>


<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
</section>




<section id="license">
<h3>各種資格コース</h3>


<div class="txt">
  <p>サンプルテキストサンプルテキスト</p>
</div>

<div class="txt summary">
<h4>レッスン概要</h4>
<p>テキストが入ります。<br>
サンプルテキストサンプルテキストサンプルテキストサンプルテキストサンプルテキスト</p>
</div>


<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
</section>

-->


<!--<section id="prsv">
<h3>プリザーブドフラワー</h3>

<p class="icon">
<img src="images/lesson/icon-sikaku.png" alt="資格">
<img src="images/lesson/icon-shumi.png" alt="趣味"></p>


<div class="txt">
  <p>アレンジをする前は、とても大切なワイヤリング、テーピングという作業があります。<br>
    デザインを考えるのは、この「下ごしらえ」の作業の間に考えます。 初心者の方は、ワイヤリング、テーピングをすることで一生懸命になってしまうので、楽しみながら、取り組んでいくと、もっと楽しさが広がりますよ。</p>
</div>

<div class="txt summary">
<h4>レッスン概要</h4>
<p>IIFA（国際フラワーアレンジメント協会）の講師の資格取得を目標とするコースです。コース内容には「ブライダルブーケコース」「クリスタルフラワーコース」「養成コース」「趣味のコース」があり、目的に応じたコースをお選び頂けます。</p>
<p>  各コースともフレックス予約制ですので、御自分の予定に合った受講スケジュールを組んで頂けます。<br>
  全国、海外で活躍してみたい方には、イベント、活動の機会も御用意できます。また、資格取得後の講師へのサポートもさせて頂きます。</p>
</div>

<div class="sakuhin">
<h4>生徒さんの作品</h4>
    <ul>
            
        <li><img src="images/lesson/sakuhin-prsv-pic-1.jpg" alt=""></li>
        <li><img src="images/lesson/sakuhin-prsv-pic-2.jpg" alt=""></li>
        <li><img src="images/lesson/sakuhin-prsv-pic-3.jpg" alt=""></li>
        <li><img src="images/lesson/sakuhin-prsv-pic-4.jpg" alt=""></li>
    
    </ul>
    
</div>


<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>
</section>-->






<!--
<section id="bridal">

<h3>ブライダルレッスン</h3>

<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>

</section>


<section id="styling">

<h3>ブライダルレッスン</h3>

<p class="center"><a href="contact"><img src="images/common/btn-contact2.png" alt="お問い合わせ・お申込みはこちら"></a></p>

</section>-->

<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->










</div><!-- lesson -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>