<?php

$item_name_id = $_GET['item_name'];

if( $_GET['item_name'] == "soapflower") { $item_name = "ソープフラワーアレンジ"; }
if( $_GET['item_name'] == "preserved") { $item_name = "プリザーブドフラワー"; }
if( $_GET['item_name'] == "artificial") { $item_name = "アーティフィシャル"; }
if( $_GET['item_name'] == "fresh") { $item_name = "フレッシュ"; }
if( $_GET['item_name'] == "interior") { $item_name = "インテリアフラワー"; }
if( $_GET['item_name'] == "mokuteki") { $item_name = "予算・目的"; }

?>
<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>受注制作（<?= $item_name ?>）｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<script type="text/javascript" src="./ci/js/top.js"></script>


</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">
<div id="main">
<div id="order">
   
<h2><img src="images/title-order.png" alt="受注制作"></h2>



<section>
<p class="mb10"><img src="images/order/catch.jpg" alt="せっかく受け取った幸運なブーケだから、永遠のインテリアに…"></p>
<div class="txt txt_catch">
<p>理創花では、お客様お一人おひとりのご要望に合わせた受注制作も承ります。 <br>
お花の種類から、ご予算や用途に合わせて、オンリーワンのオーダーメイドフラワーを作成いたします。</p>
</div>



<?php
//TOP
if( $_GET['item_name'] == "") { ?>

       <div class="navi">


        <ul>
            <li><a href="order.php?item_name=soapflower"><img src="images/order/thum-1.png" alt="">ソープフラワーアレンジ</a></li>
            <li><a href="order.php?item_name=preserved"><img src="images/order/thum-2.png" alt="">プリザーブドフラワー</a></li>
            <li><a href="order.php?item_name=artificial "><img src="images/order/thum-3.png" alt="">アーティフィシャル</a></li>
            <li><a href="order.php?item_name=fresh"><img src="images/order/thum-4.png" alt="">フレッシュ</a></li>
            <li><a href="interiorflower.php"><img src="images/order/thum-6.png" alt="">インテリアフラワー</a></li>
            <li><a href="order.php?item_name=mokuteki"><img src="images/order/thum-5.png" alt="">予算・目的</a></li>
        </ul>
        
		</div>
		        
<?php }?>



</section>




<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->

<?php
//各ページ
if( $_GET['item_name'] != "") { ?>

	<section id="<?= $item_name_id ?>">
	<h3><?= $item_name ?></h3>

	<div class="txt">
	<p>色の系統を選定します。（レッド・ブルー・ピンク・ﾊﾟｰﾌﾟﾙ・オレンジ・ホワイト・グリーン・イエローなど）<br>
	色はおまかせでも承ります。</p>
	<p>併せて、ギフト・インテリア・記念・お祝い・お悔みなどの用途もお伝えください。<br>
	</p>
	<p>商品のタイプは、器・額・ガラスドーム・花束などを制作しています。<br>
	（上記以外のタイプでも、ご遠慮なくお問い合わせください）<br>
	</p>
	<p>商品が完成しましたら、お届け、またはご来店ください。</p>
	<p>尚、ご注文はお電話、又はeメールにて、1週間前までにご連絡お願い致します。<br>
	</p>
	<p>そのほかのご注文に関しては、お気軽にお問い合わせください。</p>

	</div>

	</section>



		<section id="flow_order">
		<h3>オーダーの流れ</h3>

		<div class="flow_chart">

		<table>
		<tr>
		<th><h4>1.お花の花材をお選びください。<br>
		<span class="noB t16">（ソープフラワー、プリザーブドフラワー、アーティフィシャルフラワー<br>フレッシュ、インテリアフラワー） <br>
		お色はお任せ、または、系統を選択（レッド､ﾌﾞﾙｰ、ピンク、ﾊﾟｰﾌﾟﾙ、オレンジ，ホワイト、グリーン、イエロー、そのほか）</span></h4></th>
		</tr>
			<tr>
			  <td class="arrow"><img src="images/common/arrow.png" alt="↓"></td>
			  </tr>
			<tr>
				<th><h4>２.アレンジは、ギフト、インテリア、記念、お祝い、お悔やみ、等お選びください。<br>
		  <span class="noB t16">タイプは、器、額、ガラスドーム、花束、等</span><br>
		</h4></th>
				</tr>
			<tr>
			  <td class="arrow"><img src="images/common/arrow.png" alt="↓"></td>
			  </tr>
			<tr>
				<th><h4>3.お届け、または、来店</h4></th>
				</tr>
			<tr>
			  <td class="arrow"><img src="images/common/arrow.png" alt="↓"></td>
			  </tr>
			<tr>
				<th><h4>4.1週間前までにご連絡ください。</h4></th>
				</tr>


			<tr>
			  <td class="arrow"><img src="images/common/arrow.png" alt="↓"></td>
			
			</tr>
			<tr>
				<th><h4>5.
				  ご注文は、TEL、または、メール</h4></th>
				</tr>

			</table>





		</div>


		</section>


<?php }?>


</div><!-- lesson -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>