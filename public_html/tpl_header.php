


<h1><a href="<?= $site_url ?>"><img src="<?= $site_url ?>images/common/logo.png" alt="<?= $company_name ?>"></a></h1>

<div class="logo_flower">
<p><img src="<?= $site_url ?>images/common/logo_flower.png" alt=""></p>
</div>


<div class="tel">

<p><a href="tel:089-926-0399"><img src="<?= $site_url ?>images/common/tel.png" alt="TEL: 089-926-0399"></a></p>
<p><a href="<?= $site_url ?>contact"><img src="<?= $site_url ?>images/common/btn-contact.png" alt="お問い合わせ"></a></p>


</div>


<nav id="navi">
    <ul class="clrfix">
    <li><a href="<?= $site_url ?>"><img src="<?= $site_url ?>images/common/navi-1.png" alt="HOME"></a></li>
    <li><a href="<?= $site_url ?>information"><img src="<?= $site_url ?>images/common/navi-2.png" alt="最新情報"></a></li>
    <li><a href="<?= $site_url ?>lesson.php"><img src="<?= $site_url ?>images/common/navi-3.png" alt="レッスン"></a></li>
    <li><a href="<?= $site_url ?>order.php"><img src="<?= $site_url ?>images/common/navi-4.png" alt="受注販売"></a></li>
    <li><a href="<?= $site_url ?>dryflower.php"><img src="<?= $site_url ?>images/common/navi-5.png" alt="ドライフラワー加工"></a></li>
    <li><a href="<?= $site_url ?>preserved.php"><img src="<?= $site_url ?>images/common/navi-6.png" alt="オーダーメイド・プリザーブドフラワー"></a></li>
    <li><a href="<?= $site_url ?>orneflower.php"><img src="<?= $site_url ?>images/common/navi-9.png" alt="オルネフラワー"></a></li>
    <li><a href="<?= $site_url ?>interiorflower.php"><img src="<?= $site_url ?>images/common/navi-8.png" alt="インテリアフラワー"></a></li>
    </ul>
</nav>


<div class="bird">
<p><img src="<?= $site_url ?>images/common/bird.png" alt="Bird"></p>
</div>