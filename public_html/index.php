<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title><?php echo($site_name ) ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<script type="text/javascript" src="./ci/js/top.js"></script>


</head>
<body class="home">

<div id="bg">
<div id="bg_sub">
<div id="container">

<header>
<?php require("tpl_header.php"); ?>
</header>

<article id="content">

<div id="main">

    <div id="top">
		
		<div class="main_visual clrfix">
			<div class="img_box">
				<a href="https://www.facebook.com/%E3%83%95%E3%83%A9%E3%83%AF%E3%83%BC%E3%82%BD%E3%83%BC%E3%83%97risouka-444763312267372/" target="_blank">
				<img src="images/top/main1.jpg" alt="">
				</a>
			</div>
			<div class="img_box">
				<a href="orneflower.php">
				<img src="images/top/main2.jpg" alt="">
				</a>
			</div>
			<div class="img_box">
				<a href="chalkart.php" target="_blank">
				<img src="images/top/main3.jpg" alt="">
				</a>
			</div>
		
		</div>
    
<!--<p class="line1"><img src="images/common/line1-top.png" alt=""></p>

<div class="service_list clrfix">


        <div class="left">
        
            <h4 class="img"><img src="images/top/title-lesson.png" alt="レッスン"></h4>
            
            <ul>
                <li><a href="lesson.php#soapflower"><img src="images/top/btn-lesson1.png?20180702" alt="ソープフラワー"></a></li>
                <li><a href="lesson.php#orne"><img src="images/top/btn-lesson2.png" alt="オルネプリザーブド"></a></li>
                <li><a href="lesson.php#holland"><img src="images/top/btn-lesson3.png" alt="オランダスタイルフラワーレッスン"></a></li>
                <li><a href="lesson.php#dryflower"><img src="images/top/btn-lesson4.png" alt="ドライフラワーレッスン"></a></li>
                <li><a href="lesson.php#license"><img src="images/top/btn-lesson5.png" alt="各種資格コース"></a></li>
            </ul>

        
        </div>
    
    
        <div class="right">
        <h4 class="img"><img src="images/top/title-order.png" alt="受注制作"></h4>



        <ul>
            <li><a href="order.php?item_name=soapflower"><img src="images/top/btn-order1.png" alt="ソープフラワーアレンジ"></a></li>
            <li><a href="order.php?item_name=preserved"><img src="images/top/btn-order2.png" alt="プリザーブドフラワー"></a></li>
            <li><a href="order.php?item_name=artificial "><img src="images/top/btn-order3.png" alt="アーティフィシャル"></a></li>
            <li><a href="order.php?item_name=fresh"><img src="images/top/btn-order4.png" alt="フレッシュ"></a></li>
            <li><a href="order.php?item_name=mokuteki"><img src="images/top/btn-order5.png" alt="予算・目的"></a></li>
        </ul>


        <ul>
            <li><a href="order.php#display">お店のディスプレイ</a></li>
            <li><a href="order.php#flow_order">インテリア</a></li>
            <li><a href="order.php#mochikomi">器持ち込み</a></li>
            <li><a href="order.php#gift">ギフト</a></li>
        </ul>
        
        </div>
        
    
</div>

<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->





        
        <!--
        <div class="clrfix news_line">
            
            <div class="top_select left">
                <h3 class="img"><img src="images/top/title-select.png" alt="オーナーのセレクトPickup"></h3>
                
        
                <ul>
                    <li><span>タイトル</span><img src="images/top/select-1.jpg" alt=""></li>
                    <li><span>タイトル</span><img src="images/top/select-2.jpg" alt=""></li>
                    <li><span>タイトル</span><img src="images/top/select-3.jpg" alt=""></li>
                    <li><span>タイトル</span><img src="images/top/select-4.jpg" alt=""></li>
                    <li><span>タイトル</span><img src="images/top/select-5.jpg" alt=""></li>
                </ul>
            
            </div>
            
            
            <div class="top_information right">
            
                <h3 class="img"><img src="images/top/title-information.png"  alt="最新情報"></h3>
                <div id="news">
                </div>
            
            <p><a href="http://risouka.exblog.jp/" target="_blank"><img src="images/top/bnr_blog.jpg" alt="理創花ブログ花日記" /></a></p>
            </div>
        
        
        </div>
        -->


        <!--
        <h3 class="img mb10 center"><img src="images/top/title-concept.png" alt="" width="90%"></h3>
        <p class="mb10"><img src="images/top/image.jpg" alt="バラが咲いた。花メッセージを伝える自分らしさ花の色。これが生き方。自分らしさ。"></p>
        -->

<p class="line1"><img src="images/common/line1-top.png" alt=""></p>
		
<div class="bnr_facebook">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<p><a href="https://www.facebook.com/pages/%E3%83%95%E3%83%A9%E3%83%AF%E3%83%BC%E3%82%BD%E3%83%BC%E3%83%97risouka/444763312267372" target="_blank"><img src="images/top/bnr_facebook.gif" alt="フェイスブックページ" /></a></p>
<div class="fb-like" data-href="https://www.facebook.com/pages/%E3%83%95%E3%83%A9%E3%83%AF%E3%83%BC%E3%82%BD%E3%83%BC%E3%83%97risouka/444763312267372" data-width="220" data-layout="button_count" data-show-faces="true" data-send="true"></div>
</div>




<div class="bnr">
    
    <ul class="clrfix">
        <li><a href="http://www.komachi-wedding.com/" target="_blank"><img src="images/top/bnr-komachi.jpg" alt="Komachiウェディング"></a></li>
        <li><a href="http://www.gakusyu-forum.net/" target="_blank"><img src="images/top/bnr-gakushu.png" alt="楽習フォーラム"></a>
        <br>ハンドメイドクラフトの<br>
        プロフェッショナルを育成・輩出する。</li>
        <li><a href="http://www.amifa.jp/contribution.php/group/school" target="_blank"><img src="images/top/bnr-amifa.png" alt="amifa school"></a></li>
		<li><a href="http://orne.o.oo7.jp/" target="_blank"><img src="images/top/bnr-orne.png" alt="オルネフラワー協会" /></a></li>
	</ul>

	<ul class="clrfix">
        
		<!--<li><a href="http://r.goope.jp/savon-de-fleur" target="_blank"><img src="images/top/bnr-savon.png" alt="サボンドゥフルール協会" /></a></li>-->
		<li><a href="http://www.nfd.or.jp/" target="_blank"><img src="images/top/bnr-nfd.png" alt="" /></a></li>
		<li><a href="http://chalk-board.net/" target="_blank"><img src="images/top/bnr_chalk.png" alt="チョークボード協会" /></a></li>
		<li><img src="images/top/bnr-risouka.jpg" alt="理創花"><br>
        当サイトへのリンクはフリーです。</li>
		
    </ul>

</div>
		
		<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>
</div><!-- top -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>