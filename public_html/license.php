<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>理創花で取得できる資格｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



<!--colorbox-->
<link href="css/colorbox.css" rel="stylesheet" type="text/css" charset="UTF-8" />
<script src="js/jquery.colorbox.js"></script>
<script type="text/javascript">
$(function() {
    $('.colorbox a').colorbox({ rel: 'group' });
});
</script> 

</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">

<div id="main">
<div id="license">
    
<h2><img src="images/title-license.png" alt="理創花で取得できる資格"></h2>


<section>
<div class="list txt">

	<ul class="list-simple">
	  <li>オートクチュール・フルール（花びらメイキングコース）</li>
	  <li>オートクチュール・フルール（スタイリングコース）</li>
	  <!--<li>クチュールフローラ</li>-->
	  <!--<li>グラスフィオーレ</li>-->
	  <!--<li>プリザーブド・メイキング</li>-->
	  <!--<li>手づくりプリザーブドルネッサンス</li>-->
	  <!--<li>プリザーブドグリーン インテリア</li>-->
	  <!--<li>プリザーブドフラワー アートスキル</li>
	  <li>プリザーブドフラワー トリートメント</li>-->
	  <li>NFD（日本フラワーデザイナーズ協会）資格</li>
	  <li>オルネフラワー協会(オルネフラワー、オルネプリザーブドフラワー、他）</li>
	  <li>サボンドゥフルール協会認定講座講師資格取得 </li>
<li>マクラメジュエリー認定講座</li>
	</ul>
</div>

<!--<p class="mb10"><img src="images/license/catch.jpg" alt=""></p>
-->

    <!--<div class="txt txt_catch">
      <p>お手元のお花をプリザーブドフラワーに変身させませんか。</p>
      <p>プリザーブドフラワーは、生花と見間違えるほど生き生きしていますが、お水をあげる手間もいらない<br>
        本当に魅力的なお花です。</p>
    </div>-->
 
 
 

</section>
 
 

 
  
<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->

  
  
  
</div><!-- license -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>