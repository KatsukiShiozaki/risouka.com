<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>チョークアート｜
		<?= $site_name ?>
	</title>
	<meta name="Keywords" content="">
	<meta name="Description" content="">
	<meta http-equiv="content-script-type" content="text/javascript">
	<meta http-equiv="content-style-type" content="text/css">
	<link href="css/chalkart.css" rel="stylesheet" type="text/css">
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/chalkart.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


</head>

<body class="chalkart">
	<section class="page">

		<header>
			<div class="navi">
				<div class="logo">
					<a href="<?php echo $site_url;?>">
					<img src="images/chalkart/logo.png">
						</a>
				</div>
				<div class="menu">
					<ul>
						<li class="m_order">
					<a href="#order"><img src="images/chalkart/nav_order.png"></a></li>
						
					<li class="m_lesson">
					<a href="#lesson"><img src="images/chalkart/nav_lesson.png"></a></li>
						</ul>
				</div>
				</div>
		</header>
		<div id="container">
			<div class="main">	
			<div class="img">
				<img src="images/chalkart/main.png">
			</div>
				<h1 class="catch">
				世界に一つしかない、<strong>あなただけのチョークアート</strong>を作りませんか？
				</h1>
			</div>
			
			<div class="content_back" id="what_chalk">
				<div class="content">
					<h2>チョークアートって何？</h2>
					<div class="what_chalkart">
					<div class="left">
						
					<div class="img">
						<img src="images/chalkart/what-art.png">
						</div>
					</div>
					<div class="right">
						<div class="explain">
							チョークアートとは、チョークボード（いわゆる黒板）に描くことを言います。<br>
チョークボードペイントという専用の塗料を塗ることで、 カラフルなチョークアートを作ることが出来ます。 （左記参照。）<br><br>
							例えば、喫茶店の店内だったり、お店の前の小さな黒板にチョークで描かれたメニュー表など見たことありませんか？<br>
							チョーク特有の味のある可愛いイラストが描いてたりして、ついつい目がいってしまいますよね。<br>
							友達の家に遊びに行った時に、玄関にお花や動物などが描かれているチョークアートを見たことある方もいるのではないでしょうか。<br>
							<br>
							理創花では、そういったチョークアートを受注制作したり、チョークアートを学びたい方のために体験レッスンを行っています。
						</div>
					</div>
					</div>
				</div>


			</div>
			
			<section class="welcome">
			<div class="left">
				<div class="heading"><img src="images/chalkart/welcome-title.png"></div>
				<div class="explain">ウェルカムボードは、ご自宅の玄関やお店の入り口に置いて、
お客様を心地よくお迎えするチョークアートです。<br>
おしゃれなイメージを与えるのはもちろんのこと、見る人を惹きつけるので、集客の効果もあります。</div></div>
			<div class="right">
				<div class="img">
					<img src="images/chalkart/welcome-img.png">
				</div>
				</div>
			
			</section>
			<section class="pet">
				<div class="left">
				<div class="img"><img src="images/chalkart/pet-img.png"></div>
				</div>
				<div class="right">
					<div class="heading"><img src="images/chalkart/pet-title.png"></div>
				<div class="explain">
					ペットや食べ物、飲み物など・・・<br>
チョークアートは、たくさんの色を使って多種多様なモノを描くことが出来ます。<br>
中でもペットのチョークアートは人気で、理創花で制作のご注文される方もいますが、体験レッスンを通して、ご自身で描きたいとおっしゃる方もいらっしゃいます。</div>
				</div>
			
			</section>
			
			
			<section class="order" id="order">
			<h2>理創花では、チョークアートの受注制作を受け付けております。</h2>
				
				<section class="favorite">
				<div class="osusume">特に理創花で人気の高い注文は<span class="best">&nbsp;ウェルカムボード&nbsp;</span>と<span class="best">&nbsp;ペット&nbsp;</span>のチョークアートです。</div>
				<h3><span class="voice">『お店の前に飾って、注目を浴びるようにしたい。』<br>
『ペットは大切な家族なので、一生残る思い出として残したい。』</span><br>
など、たくさんのお声とともにご依頼頂いております。
</h3>
			
			</section>
				
				<div class="bnr">
					<a href="<?= $site_url ?>contact/?tab=toiawase#tab_navi_a">
				<img src="images/chalkart/bnr_order.png">
					</a>
				</div>
			</section>
			<section class="lesson" id="lesson">
				<h2>チョークアートの体験レッスンが出来ます。</h2>
				<h3>あなたもご自身の手でチョークアートを作ってみませんか？<br>
誰かにプレゼントしたり、大切な思い出として形に残すことが出来ます。<br>
					初めての方でも、基本からしっかりお教えしますので、一度ご体験してみてください。</h3>
				<div class="bnr">
					
					<a href="<?= $site_url ?>contact/?tab=lesson#tab_navi_a">
					<img src="images/chalkart/bnr_lesson.png">
					</a>
				</div>
			</section>
			<footer>
				<div class="copyright">Copyright© risouka.com All Rights Reserved.</div>
			</footer>
			<p class="pagetop"><a href="#wrap">トップへ</a></p>

		</section>
		


</body>
</html>