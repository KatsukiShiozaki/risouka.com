<?php
include_once("config_mail.php");
include_once("../include/config.php");
session_start();
?>
<?php
mb_language('Japanese');
mb_internal_encoding("UTF-8");

if(isset($_GET["send"])){

	if($_SESSION["mail_check"]){



		//------------------------------------------------------------------------
		//店舗へメール
		//------------------------------------------------------------------------
		$from = $_SESSION["mail_check"]["mail"];
		$to = $site_mail;
		
		
		if( $_GET['type'] == "conatct" ){ $contact_type = "お問い合わせ"; }
		if( $_GET['type'] == "lesson" ){  $contact_type = "レッスンお申込み"; }		
		
		
		$subject = $contact_type."【".$company_name."】";
		
		$body = "";
		
		$body .= "\n";
		
		$body .= "ホームページより".$contact_type."がありました。\n";
		$body .= "送信内容は下記の通りです。\n";		

		$body .= "\n";
		
		$body .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
		$body .= "■  送信内容 ■\n";
		$body .= "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n";
	

		if( $_GET['type'] == "lesson" ){ 

			$body .= "【レッスンコース】\n";
			for ($i = 0; $i < count(@$_SESSION["mail_check"]["lesson"]); $i++){
				$body .= $_SESSION["mail_check"]["lesson"][$i]."\n";
			}
			
		$body .= "\n";
		
		}

		$body .= "【お名前】\n";
		$body .= $_SESSION["mail_check"]["name"]."　様\n";
		
		$body .= "\n";
		$body .= "【お名前（フリガナ）】\n";
		$body .= $_SESSION["mail_check"]["name2"]."　様\n";

		$body .= "\n";		
	


		$body .= "【TEL】\n";
		$body .= $_SESSION["mail_check"]["tel"]."\n";	
		$body .= "\n";


		$body .= "【FAX】\n";
		$body .= $_SESSION["mail_check"]["fax"]."\n";	
		$body .= "\n";

		$body .= "【ご住所】\n";
		if($_SESSION["mail_check"]["zip"] != ""){
			$body .= "〒".$_SESSION["mail_check"]["zip"]."\n";
		}
		$body .= $_SESSION["mail_check"]["addr"]."\n";
	
		$body .= "\n";

		
		
		if($_SESSION["mail_check"]["mail"] != ""){
			$body .= "【メールアドレス】\n";
			$body .= $_SESSION["mail_check"]["mail"]."\n";
		}
		
		$body .= "\n";

		if( $_GET['type'] == "conatct" ){ 
			$body .= "【お問い合わせ内容】\n";
			$body .= $_SESSION["mail_check"]["message"]."\n";
		}
		
		
		$body .= "\n";

		
		$body .= "\n";		


			mb_send_mail($to,$subject,$body,"From:".$from);

	


		//------------------------------------------------------------------------
		//お客様へメール
		//------------------------------------------------------------------------
		
		$from = $site_mail;		
		$to = $_SESSION["mail_check"]["mail"];		
		$subject = $contact_type."確認メール【".$company_name."】";
		
		$body = "";

		
		$body .= "\n";
		$body .= $_SESSION["mail_check"]["name"]."　様\n";

		//メール文取得
		$body .= $mail_head;
		

		if( $_GET['type'] == "lesson" ){ 

			$body .= "【レッスンコース】\n";
			for ($i = 0; $i < count(@$_SESSION["mail_check"]["lesson"]); $i++){
				$body .= $_SESSION["mail_check"]["lesson"][$i]."\n";
			}

		$body .= "\n";
			
		}

		$body .= "【お名前】\n";
		$body .= $_SESSION["mail_check"]["name"]."　様\n";
		
		$body .= "\n";
		$body .= "【お名前（フリガナ）】\n";
		$body .= $_SESSION["mail_check"]["name2"]."　様\n";

		$body .= "\n";		
	


		$body .= "【TEL】\n";
		$body .= $_SESSION["mail_check"]["tel"]."\n";	
		$body .= "\n";


		$body .= "【FAX】\n";
		$body .= $_SESSION["mail_check"]["fax"]."\n";	
		$body .= "\n";

		$body .= "【ご住所】\n";
		if($_SESSION["mail_check"]["zip"] != ""){
			$body .= "〒".$_SESSION["mail_check"]["zip"]."\n";
		}
		$body .= $_SESSION["mail_check"]["addr"]."\n";
	
		$body .= "\n";

		
		
		if($_SESSION["mail_check"]["mail"] != ""){
			$body .= "【メールアドレス】\n";
			$body .= $_SESSION["mail_check"]["mail"]."\n";
		}
		
		$body .= "\n";

		if( $_GET['type'] == "conatct" ){ 
			$body .= "【お問い合わせ内容】\n";
			$body .= $_SESSION["mail_check"]["message"]."\n";
		}
		
		
		$body .= "\n";


		
		$body .= "\n";

		//署名
		$body .= $mail_sign;

		
		mb_send_mail($to,$subject,$body,"From:".$from);
		
		}


	unset($_SESSION["mail_check"]);
	header("Location: ./thanks.php");
	exit;

}
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お問い合わせ||<?php echo($site_name ) ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="../css/import.css" rel="stylesheet" type="text/css" charset="Shift_JIS">
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/contact.css" rel="stylesheet">


</head>
<body>




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("../tpl_header.php"); ?>
</header>
    





<article id="content">
<h2><img src="../images/title-contact.png" alt="お問い合わせ"></h2>

<div id="main">
<div id="contact">


<section class="box">
<h3>メールでお問い合わせ</h3>


<h4>入力内容のご確認</h4>
    <div class="txt">
    <p>入力内容にお間違いございませんでしたら、「送信」ボタンをクリックしてください</p>
    </div>

    
    <table class="tbl_form"  width="100%">

    <col width="25%">
    <col width="75%">



      <tr>
          <th>お名前</th>
          <td>
            <?php echo $_SESSION["mail_check"]["name"]?>
        </tr>
        <tr>
          <th>お名前 フリガナ</th>
          <td>
            <?php echo $_SESSION["mail_check"]["name2"]?>
            </td>
        </tr>
    
    
        <tr>
          <th>電話番号</th>
          <td>
         <?php echo $_SESSION["mail_check"]["tel"]?>
        </td>
        </tr>
    
        <tr>
          <th>ご住所</th>
          <td class="subTbl">
    〒<?php echo $_SESSION["mail_check"]["zip"]?><br />
    <?php echo $_SESSION["mail_check"]["addr"]?>
              
            </td>
        </tr>
        
        <tr>
          <th>メールアドレス</th>
          <td>
            <?php echo $_SESSION["mail_check"]["mail"]?>
        </td>
        </tr>	 
    
       
        <tr>
            <th>お問い合わせ内容</th>
            <td>
            <?php echo $_SESSION["mail_check"]["message"]?></td>
        </tr>
        
    </table>


    <div class="btn_form">
    <p>上記内容でよろしければ「送信する」をクリックしてください。</p>
    <input name="submit" type="submit" value="送信する" onClick="location.href='./check.php?send'" />
    <br />
    <p><a href="./index.php?back">修正する（前の画面に戻る）</a></p>
    </div> 

</section>


</div><!-- contact -->
</div><!-- main -->
</article><!-- content -->




<footer>
<?php require("../tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>