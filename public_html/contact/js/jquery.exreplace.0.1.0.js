/*
 * 	exReplace 0.1.0 - jQuery plugin
 *	written by Cyokodog	
 *
 *	Copyright (c) 2010 Cyokodog (http://d.hatena.ne.jp/cyokodog/)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 */
(function($){
	$.ex = $.ex || {};
	
	$.ex.replace = function(str , option ){
		var opt = $.extend({},$.ex.replace.defaults,option),
		dat = $.extend({}, opt.dat || $.ex.replace.dat,{addDat:opt.addDat});

		if(opt.datName)	opt.datName = ' ' + opt.datName + ' ';
		$.each(dat,function(idx){
			var d = dat[idx];
			if(!d || opt.datName && !(opt.datName.indexOf(' '+idx+' ')+1)) return;
			for(var i = 0;i < d.from.length; i++){
				var reg = new RegExp(d.from[i],'g');
				str = str.replace(reg,d.to[i]);	
			}
		})	
		return str;
	}
	$.ex.replace.defaults = {
		datName : null,
		addDat : null,
		dat : null
	}

	$.ex.replace.dat = {
		maru : {
			from : ['①','②','③','④','⑤','⑥','⑦','⑧','⑨','⑩','⑪','⑫','⑬','⑭','⑮','⑯','⑰','⑱','⑲','⑳','㊤','㊦','㊧','㊨','㊥'],
			to : ['(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)','(11)','(12)','(13)','(14)','(15)','(16)','(17)','(18)','(19)','(20)','(上)','(下)','(左)','(右)','(中)']
		},
		rome : {
			from : ['Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','ⅰ','ⅱ','ⅲ','ⅳ','ⅴ','ⅵ','ⅶ','ⅷ','ⅸ','ⅹ'],
			to : ['I','II','III','IV','V','VI','VII','VIII','IX','X','i','ii','iii','iv','v','vi','vii','viii','ix','x']
		},
		kana : {
			from : ['ｳﾞ','ｶﾞ','ｷﾞ','ｸﾞ','ｹﾞ','ｺﾞ','ｻﾞ','ｼﾞ','ｽﾞ','ｾﾞ','ｿﾞ','ﾀﾞ','ﾁﾞ','ﾂﾞ','ﾃﾞ','ﾄﾞ','ﾊﾞ','ﾋﾞ','ﾌﾞ','ﾍﾞ','ﾎﾞ','ﾊﾟ','ﾋﾟ','ﾌﾟ','ﾍﾟ','ﾎﾟ','ﾞ','｡','｢','｣','､','･','ｦ','ｧ','ｨ','ｩ','ｪ','ｫ','ｬ','ｭ','ｮ','ｯ','ｰ','ｱ','ｲ','ｳ','ｴ','ｵ','ｶ','ｷ','ｸ','ｹ','ｺ','ｻ','ｼ','ｽ','ｾ','ｿ','ﾀ','ﾁ','ﾂ','ﾃ','ﾄ','ﾅ','ﾆ','ﾇ','ﾈ','ﾉ','ﾊ','ﾋ','ﾌ','ﾍ','ﾎ','ﾏ','ﾐ','ﾑ','ﾒ','ﾓ','ﾔ','ﾕ','ﾖ','ﾗ','ﾘ','ﾙ','ﾚ','ﾛ','ﾜ','ﾝ','ﾟ'],
			to : ['ヴ','ガ','ギ','グ','ゲ','ゴ','ザ','ジ','ズ','ゼ','ゾ','ダ','ヂ','ヅ','デ','ド','バ','ビ','ブ','ベ','ボ','パ','ピ','プ','ペ','ポ','゛','。','「','」','、','・','ヲ','ァ','ィ','ゥ','ェ','ォ','ャ','ュ','ョ','ッ','ー','ア','イ','ウ','エ','オ','カ','キ','ク','ケ','コ','サ','シ','ス','セ','ソ','タ','チ','ツ','テ','ト','ナ','ニ','ヌ','ネ','ノ','ハ','ヒ','フ','ヘ','ホ','マ','ミ','ム','メ','モ','ヤ','ユ','ヨ','ラ','リ','ル','レ','ロ','ワ','ン','゜']
		},
		mini : {
			from : ['㈱','㈲','㈹','℡','㍾','㍽','㍼','㍻'],
			to : ['(株)','(有)','(代)','TEL','明治','大正','昭和','平成']
		}
	}

	$.fn.exReplace = function( option ){
		var targets = this;
		targets.each(function(idx){
			var target = targets.eq(idx);
			var method = /INPUT|TEXTAREA/.test(target.attr('tagName')) ? 'val' : 'html';
			target[method]($.ex.replace(target[method]() , option));
		})
	}
})(jQuery);

