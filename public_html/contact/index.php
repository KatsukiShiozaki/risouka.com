<?php
header("Content-type: text/html;charset=utf-8");
session_start();
include_once("config_mail.php");
include_once("../include/config.php");
?>
<?php
mb_language('Japanese');
mb_internal_encoding("UTF-8");

do{
if(!isset($_GET["back"])){
	if(isset($_SESSION['mail_check'])){
		unset($_SESSION["mail_check"]);
	}
}

	//チェック
	if(isset($_POST["mail_check_toiawase"])){
		
		$_SESSION["mail_check"] = $_POST;
		
		if($_POST["name"]==""){
			$_SESSION['ERROR'][err_name] = "お名前を入力してください。";
		}
		
		if($_POST["name2"]==""){
			$_SESSION['ERROR'][err_name2] = "お名前（フリガナ）を入力してください。";
		}else if (!preg_match("/^[ァ-ヶーぁ-ん　 ]+$/u", $_POST["name2"])) {
				$_SESSION['ERROR'][err_name2] = "全角「カタカナ」または「ひらがな」のみで入力してください。";
		}

		
		if($_POST["zip"]==""){
			$_SESSION['ERROR'][err_zip] = "郵便番号を入力してください。";
		}	
	
		if($_POST["addr"]==""){
			$_SESSION['ERROR'][err_addr] = "住所を入力してください。";
		}
		
		
		
		if($_POST["tel"]==""){
			$_SESSION['ERROR'][err_tel_a] = "電話番号を入力してください。";
		}
						
		if($_POST["mail"]==""){
			$_SESSION['ERROR'][err_mail] = "メールアドレスを入力してください。";
			}else
				if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_POST["mail"])) {
					if($_POST["mail"]!=$_POST["mail2"]){
						$_SESSION['ERROR'][err_mail2] = "メールアドレスが確認用と一致していません。";
					}
				 }else{				
					$_SESSION['ERROR'][err_mail] = "メールアドレスが正しくありません。";
				}
				
					
		if($_POST["message"]==""){
			$_SESSION['ERROR'][err_message] = "お問い合わせ内容を入力してください。";
		}
			


	if(isset($_SESSION['ERROR'])){
		break;
	}

	header("Location: ./check.php?send&type=conatct");
	exit;
			
	}
	//お問合せここまで
	
	
	
	

	//チェック　レッスンお申込み
	if(isset($_POST["mail_check_lesson"])){
	
		$_SESSION["mail_check"] = $_POST;
		
		if($_POST["name"]==""){
			$_SESSION['ERROR'][err_name] = "お名前を入力してください。";
		}
		
		if($_POST["name2"]==""){
			$_SESSION['ERROR'][err_name2] = "お名前（フリガナ）を入力してください。";
		}else if (!preg_match("/^[ァ-ヶーぁ-ん　 ]+$/u", $_POST["name2"])) {
				$_SESSION['ERROR'][err_name2] = "全角「カタカナ」または「ひらがな」のみで入力してください。";
		}

		
		if($_POST["zip"]==""){
			$_SESSION['ERROR'][err_zip] = "郵便番号を入力してください。";
		}	
	
		if($_POST["addr"]==""){
			$_SESSION['ERROR'][err_addr] = "住所を入力してください。";
		}
		
		
		
		if($_POST["tel"]==""){
			$_SESSION['ERROR'][err_tel_a] = "電話番号を入力してください。";
		}
						
		if($_POST["mail"]==""){
			$_SESSION['ERROR'][err_mail] = "メールアドレスを入力してください。";
			}else
				if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_POST["mail"])) {
					if($_POST["mail"]!=$_POST["mail2"]){
						$_SESSION['ERROR'][err_mail2] = "メールアドレスが確認用と一致していません。";
					}
				 }else{				
					$_SESSION['ERROR'][err_mail] = "メールアドレスが正しくありません。";
				}
				
					
		/*
		if($_POST["message"]==""){
			$_SESSION['ERROR'][err_message] = "お問い合わせ内容を入力してください。";
		}*/
			


	if(isset($_SESSION['ERROR'])){
		break;
	}

	header("Location: ./check.php?send&type=lesson");
	exit;
			
	}
	//レッスンんお申込みここまで
		
	
	

}while(0);
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お問い合わせ|<?php echo($site_name ) ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="../css/import.css" rel="stylesheet" type="text/css" charset="Shift_JIS">
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->




<link href="css/contact.css" rel="stylesheet">
<script src="js/jquery-1.2.6.js" type="text/javascript"></script>
<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>

<!--//機種依存文字変換-->
<script src="../js/jquery.exreplace-onry.js" type="text/javascript"></script>
<script src="../js/jquery.exreplace.0.1.0.js" charset="UTF-8"></script>
<script>
jQuery(function($){
	$('input,textarea').blur(function(){
		$(this).exReplace();
	});
});
</script>
<script type="text/javascript">
//エラー点滅
$(function(){
    setInterval(function(){
        $('.err_txt').fadeOut(500,function(){$(this).fadeIn(500)});
    },1000);
});
</script>






<!--ta navi-->
<script type="text/javascript">

//タブ切り替えメニュー
$(function() {
	
	$('.tab_navi li').click(function() {

		//.index()でクリックされたタブが何番目か確認
		//indexへ変数に代入
		var index = $('.tab_navi li').index(this);

		//コンテンツを一度すべて非表示
		$('.tab_content section').css('display','none');

		//クリックされたタブと同じ順番のコンテンツを表示
		$('.tab_content section').eq(index).fadeIn("slow");
		//$('.tab_content section').eq(index).css('display','block');
		
		//タブについているクラスselectを削除
		$('.tab_navi li').removeClass('select');

		//クリックされたタブのみにクラスselectを
		$(this).addClass('select');
	});
});
</script>
<!--//tab navi-->


<script>
$(document).ready(function() {
    $('.box_form').fadeIn(500);
});
</script>


</head>
<body>




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("../tpl_header.php"); ?>
</header>
    





<article id="content">
<h2><img src="../images/title-contact.png" alt="お問い合わせ"></h2>

<div id="main">
<div id="contact">
    


<section class="box" id="tel_contact">
<h3>お電話でお問い合わせ</h3>

<p class="tel_no">TEL：<?= $tel_no ?></p>





</section>


<section class="box">
<h3>メールでお問い合わせ</h3>


<?php
//問合せをデフォルトに
if($_GET["tab"] == "" ){
	$_GET["tab"] = "toiawase";
}
?>




 <div class="btn_menu clrFix display">

	<ul class="tab_navi_a" id="tab_navi_a">

        <li <?php if ($_GET["tab"] == "toiawase"){?>class="select"<?php } ?>><a href="?tab=toiawase#tab_navi_a"><img src="img/btn_mail-1.png" alt="ご予約"  /></a></li>
        
        <li <?php if ($_GET["tab"] == "lesson"){?>class="select"<?php } ?>><a href="?tab=lesson#tab_navi_a"><img src="img/btn_mail-2.png" alt="レッスン"  /></a></li>

	</ul>
    
  </div>




<div class="tab_content box_form">


<section id="toiawase_form" <?php if ($_GET["tab"] != "toiawase"){?>class="hide"<?php } ?>>
<form name="form" method="post" action="./index.php?tab=<?= $_GET["tab"] ?>#toiawase_form">


<h4>お問い合わせ</h4>

<table class="tbl_form" width="100%">

<col width="19%">
<col width="6%">
<col width="75%">

    
    <tr>
   <th>お名前</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_name])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_name]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="name" value="<?= $_SESSION["mail_check"]["name"] ?>" />
    </tr>

    <tr>
     <th>フリガナ</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_name2])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_name2]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="name2" value="<?= $_SESSION["mail_check"]["name2"]?>" />
        </td>
    </tr>
    <tr>
     <th>TEL</th>
     <th><span class="hissu">必須</span></th>
     <td>
		<?php if(isset($_SESSION['ERROR'][err_tel_a])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_tel_a]?></p><?php }?>
      <input class="txtbox sizeM" type="text" name="tel" value="<?= $_SESSION["mail_check"]["tel"]?>" />
	</td>
    </tr>
    
    <tr>
      <th>FAX</th>
      <th></th>
      <td class="subTbl">

		<?php if(isset($_SESSION['ERROR'][err_fax_a])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_fax_a]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="fax" value="<?= $_SESSION["mail_check"]["tel"]?>" />


      
      </td>
    </tr>
    <tr>
     <th>ご住所</th>
      <th><span class="hissu">必須</span></th>
      <td class="subTbl">
      <!--住所ここから-->

        <table style="margin:0">
          <tr>
            <th class="t14">郵便番号</th>
            <td><?php if(isset($_SESSION['ERROR'][err_zip])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_zip]?></p>
            <?php }?>
            <input class="txtbox sizeS" type="text" name="zip" maxlength="8" value="<?= $_SESSION["mail_check"]["zip"]?>" onKeyUp="AjaxZip3.zip2addr(this,'','addr','addr');" /></td>
          </tr>
          <tr>
            <th class="t14">ご住所<br />
            			（都道府県から）</th>
            <td><?php if(isset($_SESSION['ERROR'][err_addr])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_addr]?></p><?php }?>
				<input class="txtbox sizeL" type="text" name="addr" value="<?= $_SESSION["mail_check"]["addr"]?>" />
				　</td>
          </tr>
        </table>
		  
        </td>
    </tr>
    
    <tr>
     <th>メールアドレス</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_mail])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_mail]?></p><?php }?>
		<input class="txtbox sizeM" type="text" name="mail" value="<?= $_SESSION["mail_check"]["mail"]?>" /><br />
		<span class="t12">ご確認のため再入力してください</span><br />
		<?php if(isset($_SESSION['ERROR'][err_mail2])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_mail2]?></p><?php }?>
		<input class="txtbox sizeM" type="text" name="mail2" value="<?= $_SESSION["mail_check"]["mail2"]?>" /><br />
	</td>
    </tr>


	<tr>
    
        <th>お問い合わせ内容</th>
        <th><span class="hissu">必須</span></th>
        <td><?php if(isset($_SESSION['ERROR'][err_message])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_message]?></p><?php }?>
			<textarea name="message" style="width:400px; height:200px;"><?= $_SESSION["mail_check"]["message"]?></textarea>
			</td>
        
	</tr>
    
    
</table>



<div class="btn_form">
<input type="submit" name="mail_check_toiawase" value="送信する" />
</div>

</form>
</section>




<section id="lesson_form" <?php if ($_GET["tab"] != "lesson"){?>class="hide"<?php } ?>>
<form name="form" method="post" action="./index.php?tab=<?= $_GET["tab"] ?>#lesson_form">

<h4>レッスンお申込み</h4>


<table class="tbl_form" width="100%">

<col width="19%">
<col width="6%">
<col width="75%">


    
   <tr>
     <th>レッスンコース</th>
     <th><span class="hissu">必須</span></th>
     <td>
     
<label><input type="checkbox" name="lesson[]" value="ソープフラワーレッスン" <?php if (in_array('ソープフラワー資格', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> ソープフラワーレッスン</label>
<label><input type="checkbox" name="lesson[]" value="オルネフラワー協会" <?php if (in_array('オルネフラワー協会', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> オルネフラワー協会</label>
<label><input type="checkbox" name="lesson[]" value="ドライフラワーレッスン" <?php if (in_array('ドライフラワーレッスン', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> ドライフラワーレッスン</label>
<label><input type="checkbox" name="lesson[]" value="オランダスタイル" <?php if (in_array('オランダスタイル', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> オランダスタイル</label>
<label><input type="checkbox" name="lesson[]" value="学習フォーラムカルチャーレッスン" <?php if (in_array('学習フォーラムカルチャーレッスン', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> 学習フォーラムカルチャーレッスン</label>
<label><input type="checkbox" name="lesson[]" value="オートクチュール・フルール認定講座" <?php if (in_array('オートクチュール・フルール認定講座', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> オートクチュール・フルール認定講座</label>
		 <label><input type="checkbox" name="lesson[]" value="チョークアート体験レッスン" <?php if (in_array('チョークアート体験レッスン', $_SESSION["mail_check"]["lesson"])){ ?>checked="checked"<?php }?>> チョークアート体験レッスン</label>

     </td>
    </tr>
    
    <tr>
   <th>お名前</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_name])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_name]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="name" value="<?= $_SESSION["mail_check"]["name"] ?>" />
    </tr>

    <tr>
     <th>フリガナ</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_name2])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_name2]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="name2" value="<?= $_SESSION["mail_check"]["name2"]?>" />
        </td>
    </tr>
    <tr>
     <th>TEL</th>
     <th><span class="hissu">必須</span></th>
     <td>
		<?php if(isset($_SESSION['ERROR'][err_tel_a])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_tel_a]?></p><?php }?>
      <input class="txtbox sizeM" type="text" name="tel" value="<?= $_SESSION["mail_check"]["tel"]?>" />
	</td>
    </tr>
    
    <tr>
      <th>FAX</th>
      <th></th>
      <td class="subTbl">

		<?php if(isset($_SESSION['ERROR'][err_fax_a])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_fax_a]?></p><?php }?>
        <input class="txtbox sizeM" type="text" name="fax" value="<?= $_SESSION["mail_check"]["fax"]?>" />


      
      </td>
    </tr>
    <tr>
     <th>ご住所</th>
      <th><span class="hissu">必須</span></th>
      <td class="subTbl">
      <!--住所ここから-->

        <table style="margin:0">
          <tr>
            <th class="t14">郵便番号</th>
            <td><?php if(isset($_SESSION['ERROR'][err_zip])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_zip]?></p>
            <?php }?>
            <input class="txtbox sizeS" type="text" name="zip" maxlength="8" value="<?= $_SESSION["mail_check"]["zip"]?>" onKeyUp="AjaxZip3.zip2addr(this,'','addr','addr');" /></td>
          </tr>
          <tr>
            <th class="t14">ご住所<br />
            			（都道府県から）</th>
            <td><?php if(isset($_SESSION['ERROR'][err_addr])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_addr]?></p><?php }?>
				<input class="txtbox sizeL" type="text" name="addr" value="<?= $_SESSION["mail_check"]["addr"]?>" />
				　</td>
          </tr>
        </table>
		  
        </td>
    </tr>
    
    <tr>
     <th>メールアドレス</th>
      <th><span class="hissu">必須</span></th>
      <td>
		<?php if(isset($_SESSION['ERROR'][err_mail])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_mail]?></p><?php }?>
		<input class="txtbox sizeM" type="text" name="mail" value="<?= $_SESSION["mail_check"]["mail"]?>" /><br />
		<span class="t12">ご確認のため再入力してください</span><br />
		<?php if(isset($_SESSION['ERROR'][err_mail2])){ ?><p class="err_txt"><?= $_SESSION['ERROR'][err_mail2]?></p><?php }?>
		<input class="txtbox sizeM" type="text" name="mail2" value="<?= $_SESSION["mail_check"]["mail2"]?>" /><br />
	</td>
    </tr>


    
</table>



<div class="btn_form">
<input type="submit" name="mail_check_lesson" value="送信する" />
</div>

</form>

</section>




<?php
unset($_SESSION['ERROR']);
?>  


</div><!--box_form-->







</section>



</div><!-- contact -->
</div><!-- main -->
</article><!-- content -->




<footer>
<?php require("../tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>
