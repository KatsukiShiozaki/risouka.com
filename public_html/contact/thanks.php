<?php
include_once("config_mail.php");
include_once("../include/config.php");
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>お問い合わせ||<?php echo($site_name ) ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="../css/import.css" rel="stylesheet" type="text/css" charset="Shift_JIS">
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link href="css/contact.css" rel="stylesheet">


</head>
<body>




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("../tpl_header.php"); ?>
</header>
    





<article id="content">
<h2><img src="../images/title-contact.png" alt="お問い合わせ"></h2>

<div id="main">
<div id="contact">


<section class="box">
<h3>メールでお問い合わせ</h3>



<h4>送信完了しました</h4>
<div class="txt">
<p><br>			
 このたびは、お問い合わせいただきまして、有難うございます。<br />
			お問い合わせ内容をご確認の上、折り返しご連絡させていただきます。<br />
	  何卒よろしくお願いいたします。</p>
<p>自動で確認メールをお送りしておりますので、ご確認ください。<br />
			確認メールが届かない場合は、メールアドレスが間違っている恐れがございますので、<br />
			メールアドレスをご確認の上、再度お問い合わせください。<br />
	  また、迷惑メールフォルダなどに入っている場合がございます。<br />
			<br />
			<br />
			<br />
			<br />
			<br />
</p>
</div>


</section>


</div><!-- contact -->
</div><!-- main -->
</article><!-- content -->

<footer>
<?php require("../tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>