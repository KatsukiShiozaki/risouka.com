<?php
class Login extends Controller {
    function Login(){
        parent::Controller();
        $this->load->library(array('simplelogin', 'validation', 'session'));
        $this->load->helper(array('url', 'form'));
        $this->load->database();
    }

    function index() {
        if ($this->session->userdata('logged_in')) {
            redirect('admin/blog', 'refresh');
        }
        else if ($this->input->post('username')) {
            $result = $this->simplelogin->login($this->input->post('username'), $this->input->post('password'));

            if($result == TRUE){
                redirect('admin/blog', 'refresh');
            }else{
                redirect('admin/login', 'refresh');
            }
        }
        else {
            $data = array(
                'title'    => 'Login',
                'base_url' => base_url(),
                'css_list' => array('login.css'),
                'js_list' => array(),
                );
            $this->load->view('admin/head.tpl', $data);
            $this->load->view('admin/login.tpl', $data);
            $this->load->view('admin/foot.tpl');
        }
    }

    function logout(){
        $this->simplelogin->logout();
        redirect('admin/login', 'location');
    }
}
?>
