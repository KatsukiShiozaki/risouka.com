<?php
class Category extends Controller {
    function Category() {
        parent::Controller();
        $this->load->library(array('simplelogin', 'validation', 'session'));
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
        if (!$this->session->userdata('logged_in')) {
            redirect('admin/login', 'refresh');
        }
    }

    function sort_update() {
        $sorted_ids = $this->input->post('sorted_ids');
        $ids = explode(',', $sorted_ids);
        $i = 0;
        foreach ($ids as $id) {
            if (preg_match('/(\d+)$/', $id, $matches)) {
                $this->db->where('id', $matches[1]);
                $this->db->update('category', array('indication_order' => $i));
                $i++;
            }
        }
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }

    function update() {
        $id = $this->input->post('id', true);
        $data = array('name' => $this->input->post('name'));
        $this->db->where('id', $id);
        $this->db->update('category', $data);
        $row = $this->db->get_where('category', array('id' => $id))->row();
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'row' => $row));
    }

    function add() {
        $data = array('name' => $this->input->post('name'));
        $this->db->insert('category', $data);
        $id = $this->db->insert_id();
        $this->db->where('id', $id);
        $this->db->update('category', array('indication_order' => $id));
        $row = $this->db->get_where('category', array('id' => $id))->row();
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'row' => $row));
    }

    function get_list() {
        $category_rows = $this->db->get('category')->result_array();
        $blog_rows     = $this->db->get('blog')->result_array();
        $use_ids = array();
        foreach ($blog_rows as $row) {
            $use_ids[$row['category_id']] = 1;
        }
        $rows = array();
        foreach ($category_rows as $row) {
            if (isset($use_ids[$row['id']])) {
                $row['is_using'] = '1';
            }
            else {
                $row['is_using'] = '0';
            }
            $rows[] = $row;
        }
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'rows' => $rows));
    }

    function remove() {
        $this->db->where('id', $this->input->post('id', true));
        $this->db->delete('category');
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }
}
?>
