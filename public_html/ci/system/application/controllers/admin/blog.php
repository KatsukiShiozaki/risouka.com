<?php
class Blog extends Controller {
    function Blog() {
        parent::Controller();
        $this->load->library(array('simplelogin', 'validation', 'session'));
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
        if (!$this->session->userdata('logged_in')) {
            redirect('admin/login', 'refresh');
        }
        $this->file_path = './images/photo/';
    }

    function index() {
        $data = array(
            'title'    => 'ブログ 管理',
            'base_url' => base_url(),
            'css_list' => array('admin/blog.css'),
            'js_list'  => array('ui.datepicker-ja.js', 'admin/blog.js', 'admin/category.js')
        );
        $this->load->view('admin/head.tpl', $data);
        $this->load->view('admin/menu.tpl', $data);
        $this->load->view('admin/blog.tpl', $data);
        $this->load->view('admin/foot.tpl');
    }

    function update() {
        $id = $this->input->post('id', true);
        $data = array(
            'title'       => $this->input->post('title'),
            'date'        => $this->input->post('date'),
            'text'        => $this->input->post('text'),
            'category_id' => $this->input->post('category_id')
        );
        $this->db->where('id', $id);
        $this->db->update('blog', $data);
        $row = $this->db->get_where('blog', array('id' => $id))->row_array();

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'row' => $row));
    }

    function get_image_data($id = 0) {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        }

        $res_dir = opendir($this->file_path);
        $data = array('images' => array(), 'max_number' => 1);
        $images = array();
        $pattern = '/^' . $id . '_(\\d+)\\.jpg$/';
        while($file_name = readdir($res_dir)){
            if (preg_match($pattern, $file_name, $matches)) {
                $images[] = $file_name;
                if ($matches[1] > $data['max_number']) {
                    $data['max_number'] = $matches[1];
                }
            }
        }
        closedir($res_dir);
        $data['max_number']++;
        natsort($images);
        $images = array_reverse($images);
        if ($images) {
            foreach ($images as $img) {
                $data['images'][] = $img;
            }
        }
        return $data;
    }

    function add_category() {
        $data = array('name' => $this->input->post('name'));
        $this->db->insert('category', $data);
        $id = $this->db->insert_id();
        $this->db->where('id', $id);
        $this->db->update('category', array('indication_order' => $id));
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }

    function add() {
        $text = $this->input->post('text');
        $this->db->insert('blog', array(
                'title'       => $this->input->post('title'),
                'date'        => $this->input->post('date'),
                'text'        => $text,
                'category_id' => $this->input->post('category_id')
            ));
        $id = $this->db->insert_id();

        $replace = '${2}' . $id . '$4';
        $text = preg_replace('/((\[IMG )(\d+)(_\d+\.jpg\]))/', $replace, $text);
        $data = array('text' => $text);
        $this->db->where('id', $id);
        $this->db->update('blog', $data);

        $image_data = $this->get_image_data(0);
        $pattern = '/^' . '0_(\\d+)\\.jpg$/';
        $res_dir = opendir($this->file_path);
        while($file_name = readdir($res_dir)){
            if (preg_match($pattern, $file_name, $matches)) {
                rename($this->file_path . '0_' . $matches[1] . '.jpg', $this->file_path . $id . '_' . $matches[1] . '.jpg');
            }
        }
        closedir($res_dir);

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }

    function get_image() {
        $id = $this->input->post('id', true);
        $image_data = $this->get_image_data($id);

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'list' => $image_data['images'], 'max_number' => $image_data['max_number']));
    }

    function upload() {
        $id     = $this->input->post('id', true);
        $number = $this->input->post('number', true);
        $config['upload_path']   = './images/photo/';
        $config['allowed_types'] = 'jpg';
        $config['overwrite']     = true;
        $config['file_name']     = $id . '_' . $number;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo')) {
          $error = array('error' => $this->upload->display_errors('', ''));
          var_dump($error);
          echo '<html>'
              . '<body>'
              . '<script type="text/javascript">'
              . 'alert("' . implode('', $error) . '")'
              . '</script>'
              . '</body>'
              . '</html>'
              ;
        }
        else {
          $data = array('upload_data' => $this->upload->data());
          echo '<html>'
              . '<body>'
              . '<script type="text/javascript">'
              . 'window.top.blogObj.afterUpload("' . $id . '", "' . $number . '");'
              . '</script>'
              . '</body>'
              . '</html>'
              ;
        }
    }

    function get_rows() {
        $rows = $this->db->get('blog')->result_array();
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'rows' => $rows));
    }

    function remove_photo() {
        $file = $this->input->post('file', true);
        $file = './images/photo/' . $file;
        if (file_exists($file)) {
            unlink($file);
        }
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }

    function remove() {
        $id = $this->input->post('id', true);
        $this->db->where('id', $id);
        $this->db->delete('blog');
        $image_data = $this->get_image_data($id);
        if ($image_data['images']) {
            foreach ($image_data['images'] as $image) {
                $file = './images/photo/' . $image;
                unlink($file);
            }
        }
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }
}
?>
