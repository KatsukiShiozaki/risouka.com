<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/admin/jquery/ui-darkness/jquery-ui-1.8.5.custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/default.css">
<?php foreach ($css_list as $css): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/<?php echo $css; ?>">
<?php endforeach; ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.min.js"></script>
<?php foreach ($js_list as $js): ?>
    <script type="text/javascript" src="<?php echo $base_url; ?>js/<?php echo $js; ?>"></script>
<?php endforeach; ?>
    <title>理創花　管理サイト　<?= $title ?></title>
  </head>
  <body class="yui-skin-sam">
    <div id="container">
    <h1>理創花　管理サイト　<?= $title ?></h1>

