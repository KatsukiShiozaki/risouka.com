<script src="http://maps.google.com/maps/api/js?sensor=false&language=ja"></script>
<div class="content">
  <div class="update_forms">
    <div id="photo_display">
      <?php if (file_exists('./images/top_picture/top_picture.jpg')): ?>
      <img src="../images/top_picture/top_picture.jpg" alt="" />
      <?php endif; ?>
    </div>
    <form id="photo_form" action="top_picture/upload" target="upload_target" method="post" enctype="multipart/form-data">
      <input name="photo" id="photo" type="file" title="写真は「JEPG」のみ登録できます。" accept="image/jpeg" />
    </form>
    <form action="top_picture/update" method="post">
      <div>
        <textarea name="message" id="message" rows="5" cols="5"><?php echo $message; ?></textarea>
      </div>
      <div class="buttons">
        <div id="update_message"></div>
        <input name="ok_button" id="ok_button" type="button" value="登録" />
      </div>
    </form>
  </div>
</div>
<div id="calContainer" style="display: none;"></div>
<iframe id="upload_target" name="upload_target" src="" style="width:0; height:0; border: 0px solid #fff;"></iframe>
