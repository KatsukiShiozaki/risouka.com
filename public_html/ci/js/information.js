var information = function() {
  var that = {
    reBr: /(\r)?\n/g,
    reImg: /\[IMG (\d+_\d+.jpg)\]/g,
    reId: /(\d+)$/,
    reYearMonth: /(\d{4})_(\d{2})/,
    reArchive: /(\d{4}_\d{2})$/,
    rows: [],
    categoryRows: [],
    archiveList: [],
    rowsCount: 0,
    nowPage: 1,
    nowCategory: 0,
    nowArchive: '',
    getRows: function() {
      jQuery.ajax({
        url: '../ci/information/get/' + that.nowPage + '/' + that.nowCategory + '/' + that.nowArchive,
        type: 'POST',
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.rows         = json.rows;
            that.categoryRows = json.category_rows;
            that.archiveList  = json.year_month_list;
            that.rowsCount    = json.count;
            that.setRows();
            that.createPager();
            that.createMenu();
          }
        }
      });
    },
    createMenu: function() {
      // var contents = '<h4>Category</h4><ul>';
      // jQuery.each(that.categoryRows, function(i, row) {
      //   contents += '<li class="menu_category" id="menu_category_' + row.id  + '">' + row.name + '</li>';
      // });
      // contents += '</ul>';
      //
      // contents += '<h4>Archives</h4><ul>';
      var contents = '<h4>Archives</h4><ul>';
      jQuery.each(that.archiveList.sort(function(rowA, rowB) {
        if (rowB > rowA) {
          return 1;
        }
        else {
          return -1;
        }
      }), function(i, row) {
        var yearMonth = row.replace(that.reYearMonth, '$1年$2月');
        contents += '<li class="menu_archive" id="menu_archive_' + row  + '">' + yearMonth + '</li>';
      });
      contents += '</ul>';

      jQuery('#blog_menu').html(contents);
    },
    setRows: function() {
      var contents = '';
      jQuery.each(that.rows, function(i, row) {
        var text = row.text.replace(that.reBr, '<br />');
        text = text.replace(that.reImg, '<img src="../ci/images/photo/$1" alt="" />');
        contents += ''
        + '<div class="information" id="news_' + row.id + '">'
        + '  <h4>' + row.title + '</h4>'
        + '  <div class="date">' + row.date + '</div>'
        + '  <p>' + text + '</p>'
        + '</div>'
        + '<p class="page_top"><a href="#"><img src="../images/common/btn_page_top.jpg" alt="このページのTOPへ"  /></a></p>'
        ;
      });
      jQuery('#main_box').html(contents);
      that.checkUrl();
    },
    createPager: function() {
      var pageCount = 0;
      if (that.rowsCount) {
        pageCount = Math.floor((that.rowsCount - 1) / 5) + 1;
      }
      var contents = '';
      for (var i = 1; i <= pageCount; i++) {
        if (i === that.nowPage) {
          contents += '<span class="now">' + i + '</span>';
        }
        else {
          contents += '<span class="page" id="page_' + i + '">' + i + '</span>';
        }
      }
      jQuery('#pager').html(contents);
    },
    eventPager: function(e) {
      var elem = jQuery(e.target).closest('span.page');
      if (elem.length) {
        var result = that.reId.exec(elem.attr('id'));
        that.nowPage = result[1] - 0;
        that.getRows();
      }
    },
    eventMenu: function(e) {
      var elem = jQuery(e.target).closest('li.menu_category');
      if (elem.length) {
        var result = that.reId.exec(elem.attr('id'));
        that.nowPage = 1;
        that.nowCategory = result[1] - 0;
        that.nowArchive = '';
        that.getRows();
      }
      elem = jQuery(e.target).closest('li.menu_archive');
      if (elem.length) {
        var result = that.reArchive.exec(elem.attr('id'));
        that.nowPage = 1;
        that.nowCategory = 0;
        that.nowArchive = result[1];
        that.getRows();
      }
    },
    checkUrl: function() {
      var reUrl = /\?(.+)$/;
      var result = reUrl.exec(decodeURIComponent(location.search));
      // if (result[1]) {
      //   var targetOffset = $('#' + result[1]).offset().top;
      //   jQuery('html,body').animate({ scrollTop: targetOffset });
      // }
    }
  };
  return that;
};

var informationObj = new information();
jQuery(document).ready(function() {
  jQuery('#pager').bind('click', informationObj.eventPager);
  jQuery('#blog_menu').bind('click', informationObj.eventMenu);
  informationObj.getRows();
});
