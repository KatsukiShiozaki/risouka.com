var category = function() {
  var that = {
    rows: [],
    reId: /(\d+)/,
    addId: '',
    editId: '',
    removeId: '',
    update: function() {
      var action = './category/add';
      var data = { name: jQuery('#edit_category').val() };
      var mode = 'add';
      if (that.editId !== '') {
        action = './category/update';
        data.id = that.editId;
        mode = 'update';
      }
      jQuery.ajax({
        url: action,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            var updateRow = json.row;
            if (mode === 'update') {
              var newRows = [];
              jQuery.each(that.rows, function(i, row){
                if (row.id === that.editId) {
                  row = updateRow;
                }
                newRows.push(row);
              });
              that.rows = newRows;
              var row = updateRow;
              var contents = ''
              + '<img src="../images/admin/object-flip-vertical.png" alt="" title="ドラッグして順番を変更できます。" />'
              + '<span class="name">' + row.name + '</span>' 
              + '<span class="edit" id="edit_' + row.id + '">編集</span>' 
              + '<span class="remove" id="remove_' + row.id + '">削除</span>' 
              ;
              jQuery('#category_row_' + that.editId).html(contents);
              that.setSortable();
              that.editId = '';
              jQuery('#disp_category_name').html(row.name);
              jQuery('#category_edit_block').fadeOut(null, function() { jQuery('#category_select_block').fadeIn(); });
            }
            else if (mode === 'add') {
              that.addId = updateRow.id;
              that.rows.push(updateRow);
              that.setRows();
              jQuery('#category_edit_block').fadeOut(null, function() { jQuery('#category_select_block').fadeIn(); });
            }
          }
        }
      });
    },
    remove: function() {
      jQuery.ajax({
        url: './category/remove',
        type: 'POST',
        data: { id: that.removeId },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            jQuery('#category_remove_confirm').fadeOut(null, function() {
              jQuery('#category_row_' + that.removeId).fadeOut(null, function() {
                jQuery('#category_row_' + that.removeId).slideUp();
              });
            });
            that.rows = jQuery.grep(that.rows, function(row) { return row.id !== that.removeId });
          }
        }
      });
    },
    add: function() {
      that.editId = '';
      jQuery('#edit_category').val('');
      jQuery('#category_select_block').fadeOut(null, function() { jQuery('#category_edit_block').fadeIn(); });
    },
    getRows: function() {
      jQuery.ajax({
        url: './category/get_list',
        type: 'POST',
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.rows = json.rows;
            that.setRows();
          }
        }
      });
    },
    setRows: function() {
      var contents = '';
      jQuery.each(that.rows.sort(function(rowA, rowB) { return rowA.indication_order - rowB.indication_order; }), function(i, row) {
        var remove = '<span class="remove" id="remove_' + row.id + '">削除</span>';
        if (row.is_using === '1') {
          remove = '<span class="remove_using">削除</span>';
        }
        contents += ''
        + '<li class="category_row" id="category_row_' + row.id + '">'
        + '<img src="../images/admin/object-flip-vertical.png" alt="" title="ドラッグして順番を変更できます。" />'
        + '<span class="name">' + row.name + '</span>' 
        + '<span class="edit" id="edit_' + row.id + '">編集</span>'
        + remove
        + '</li>'
        ;
      });
      jQuery('#category_rows').html(contents);

      that.setSortable();

      jQuery('<li id="category_all"><span class="name">全てのカテゴリ</span></li>').prependTo('#category_rows');
      jQuery('#category_all').bind('click', function() {
        that.editId = '';
        jQuery('#disp_category_name').html('全てのカテゴリ');
        jQuery('#category_rows').fadeOut();
        blogObj.setRows();
      });
    },
    setSortable: function() {
      jQuery("#category_rows").sortable({
        handle: 'img',
        update: function(event, ui) {
          var sort_result = jQuery('#category_rows').sortable('toArray').toString();
          jQuery.ajax({
            url: './category/sort_update',
            type: 'POST',
            data: { sorted_ids: sort_result },
            dataType: 'json',
            success: function(json) {
              if (json.result === 'okay') {
              }
            }
          });
        }
      }).disableSelection();
    },
    rowsEvent: function(e) {
      var elemRemove      = jQuery(e.target).closest('span.remove');
      var elemRemoveUsing = jQuery(e.target).closest('span.remove_using');
      var elemEdit        = jQuery(e.target).closest('span.edit');
      var elemRow         = jQuery(e.target).closest('li.category_row');
      if (elemRemove.length) {
        var result = that.reId.exec(elemRemove.attr('id'));
        that.removeId = result[1];
        var targetRow = {};
        jQuery.each(that.rows, function(i, row) {
          if (row.id === that.removeId) {
            targetRow = row;
            return false;
          }
        });
        jQuery('#category_remove_title').html(targetRow.name);
        var pos = elemRemove.offset();
        jQuery('#category_remove_confirm').css({ top: pos.top - 50, left: pos.left - 150 });
        jQuery('#category_remove_confirm').fadeIn();
      }
      else if (elemEdit.length) {
        var result = that.reId.exec(elemEdit.attr('id'));
        that.editId = result[1];
        var targetRow = {};
        jQuery.each(that.rows, function(i, row) {
          if (row.id === that.editId) {
            targetRow = row;
            return false;
          }
        });
        jQuery('#disp_category_name').html(targetRow.name);
        jQuery('#edit_category').val(targetRow.name);
        jQuery('#category_rows').fadeOut(null, function() {
          jQuery('#category_select_block').fadeOut(null, function() {
            jQuery('#category_edit_block').fadeIn();
          });
        });
      }
      else if (elemRemoveUsing.length) {
        return;
      }
      else if (elemRow.length) {
        if (jQuery(e.target).closest('span.name').length) {
          var result = that.reId.exec(elemRow.attr('id'));
          that.editId = result[1];
          var targetRow = {};
          jQuery.each(that.rows, function(i, row) {
            if (row.id === that.editId) {
              targetRow = row;
              return false;
            }
          });
          jQuery('#disp_category_name').html(targetRow.name);
          jQuery('#category_rows').fadeOut();
          blogObj.setRows();
        }
      }
    },
    cancelRemove: function() {
      that.removeId = '';
      jQuery('#category_remove_confirm').fadeOut();
    },
    showRows: function(e) {
      e.stopImmediatePropagation();
      jQuery('#category_rows').animate({ opacity: 'toggle' });
    },
    hideRows: function(e) {
      var elem1 = jQuery(e.target).closest('#category_rows');
      var elem2 = jQuery(e.target).closest('#category_remove_confirm');
      if (elem1.length === 0 && elem2.length === 0) {
        jQuery('#category_rows').fadeOut()
      }
    }
  };
  return that;
};

var categoryObj = new category();
jQuery(document).ready(function() {
  jQuery('#category_rows').bind('click', categoryObj.rowsEvent);
  jQuery('#category_remove_cancel').bind('click', categoryObj.cancelRemove);
  jQuery('#category_remove_ok').bind('click', categoryObj.remove);
  jQuery('#category_edit_ok').bind('click', categoryObj.update);
  jQuery('#category_add').bind('click', categoryObj.add);
  jQuery('#category_edit_cancel').bind('click', function() {
    jQuery('#category_edit_block').fadeOut(null, function() {
      categoryObj.editId = '';
      jQuery('#edit_category').val('');
    });
    jQuery('#category_edit_block').fadeOut(null, function() {
      categoryObj.editId = '';
      jQuery('#edit_category').val('');
      jQuery('#category_select_block').fadeIn();
    });
  });
  jQuery('#disp_category').bind('click', categoryObj.showRows);
  jQuery(window.document).bind('click', categoryObj.hideRows);
  categoryObj.getRows();
})
