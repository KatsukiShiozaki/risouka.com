var blog = function() {
  var that = {
    rows: [],
    reId: /(\d+)/,
    reImage: /(\d+_\d+.jpg)/,
    reIdNumber: /(\d+)_(\d+)$/,
    reIdNumberByImage: /(\d+)_(\d+)\.jpg/,
    addCategoryId: '',
    editCategoryId: '',
    removeCategoryId: '',
    addId: '',
    editId: '0',
    removeId: '',
    update: function() {
      var action = './blog/add';
      var data = {
        date:        jQuery('#date').val(),
        title:       jQuery('#title').val(),
        text:        jQuery('#text').val(),
        category_id: jQuery('#category_id').val()
      };
      if (that.editId !== '0') {
        action = './blog/update';
        data.id = that.editId;
      }
      jQuery.ajax({
        url: action,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            jQuery('#edit_block').fadeOut();
            that.getRows();
          }
        }
      });
    },
    removePhoto: function(id, number) {
      jQuery.ajax({
        url: './blog/remove_photo',
        type: 'POST',
        data: { file: id + '_' + number + '.jpg' },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            jQuery('#photo_id_' + id + '_' + number).animate({ opacity: 0.1 }, 'slow', function() {
              jQuery('#photo_id_' + id + '_' + number).slideUp('slow');
            });
          }
        }
      });
    },
    remove: function() {
      jQuery.ajax({
        url: './blog/remove',
        type: 'POST',
        data: { id: that.removeId },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            jQuery('#remove_confirm').fadeOut();
            that.getRows();
          }
        }
      });
    },
    add: function() {
      that.editId = '0';
      var options = '<option value="0">選択</option>';
      jQuery.each(categoryObj.rows, function(i, row) {
          if (categoryObj.editId === row.id) {
            options += '<option value="' + row.id + '" selected="selected">' + row.name + '</option>';
          }
          else {
            options += '<option value="' + row.id + '">' + row.name + '</option>';
          }
      });
      jQuery('#date').val('');
      jQuery('#title').val('');
      jQuery('#text').val('');
      var pos = jQuery('#add').position();
      jQuery('#category_id').html(options);
      jQuery('#edit_block').css({ top: pos.top, left: pos.left });
      jQuery('#edit_block').fadeIn();
      that.editId = '0';
      that.getImage();
    },
    getRows: function() {
      jQuery.ajax({
        url: './blog/get_rows',
        type: 'POST',
        data: { id: that.removeId },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.rows = json.rows;
            that.setRows();
          }
        }
      });
    },
    setRows: function() {
      jQuery('#rows').html('');
      jQuery.each(that.rows.sort(function(rowA, rowB) {
        if (rowB.date > rowA.date) {
          return 1;
        }
        else if (rowB.date < rowA.date) {
          return -1;
        }
        else {
          if (rowB.update_time > rowA.update_time) {
            return 1;
          }
          else {
            return -1;
          }
        }
      }), function(i, row) {
        if (categoryObj.editId !== '' && categoryObj.editId !== row.category_id) {
          return;
        }
        var contents = ''
        + '<li id="row_' + row.id + '">\n'
        + '<span class="date">' + row.date + '</span>\n' 
        + '<span class="title">' + row.title + '</span>\n' 
        + '<span class="edit" id="edit_' + row.id + '">編集</span>\n' 
        + '<span class="remove" id="remove_' + row.id + '">削除</span>\n' 
        + '</li>'
        ;
        jQuery(contents).appendTo('#rows');
      });
    },
    rowsEvent: function(e) {
      var elem = jQuery(e.target).closest('span.remove');
      if (elem.length) {
        var result = that.reId.exec(elem.attr('id'));
        that.removeId = result[1];
        var targetRow = {};
        jQuery.each(that.rows, function(i, row) {
          if (row.id === that.removeId) {
            targetRow = row;
            return false;
          }
        });
        jQuery('#remove_title').html(targetRow.title);
        var pos = elem.position();
        jQuery('#remove_confirm').css({ top: pos.top, left: pos.left });
        jQuery('#remove_confirm').fadeIn();
      }
      elem = jQuery(e.target).closest('span.edit');
      if (elem.length) {
        var result = that.reId.exec(elem.attr('id'));
        that.editId = result[1];
        var targetRow = {};
        jQuery.each(that.rows, function(i, row) {
          if (row.id === that.editId) {
            targetRow = row;
            return false;
          }
        });
        var options = '<option value="0">選択</option>';
        jQuery.each(categoryObj.rows, function(i, row) {
          if (targetRow.category_id === row.id) {
            options += '<option value="' + row.id + '" selected="selected">' + row.name + '</option>';
          }
          else {
            options += '<option value="' + row.id + '">' + row.name + '</option>';
          }
        });
        jQuery('#category_id').html(options);
        jQuery('#date').val(targetRow.date)
        jQuery('#title').val(targetRow.title);
        jQuery('#text').val(targetRow.text);
        var offset = elem.offset();
        jQuery('#edit_block').css({ top: offset.top - 20 });
        jQuery('#edit_block').fadeIn();
        that.getImage();
      }
    },
    photoListEvent: function(e) {
      var elem = jQuery(e.target).closest('span.remove');
      if (elem.length) {
        var result = that.reIdNumber.exec(elem.attr('id'));
        that.removePhoto(result[1], result[2]);
      }
      elem = jQuery(e.target).closest('span.insert');
      if (elem.length) {
        var result = that.reImage.exec(elem.attr('id'));
        jQuery('#text').insertAtCaret('[IMG ' + result[1] + ']');
      }
    },
    cancelRemove: function() {
      that.removeId = '';
      jQuery('#remove_confirm').fadeOut();
    },
    getImage: function() {
      jQuery.ajax({
        url: './blog/get_image',
        type: 'POST',
        data: { id: that.editId },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.imageList = json.list;
            that.maxImageNumber = json.max_number;
            that.setImage();
          }
        }
      });
    },
    setImage: function() {
      var contents = '';
      jQuery.each(that.imageList, function(i, image) {
        var result = that.reIdNumberByImage.exec(image);
        var id = result[1];
        var number = result[2];
        var elemId = 'photo_id_' + id + '_' + number;
        contents += '<li id="' + elemId + '">'
        + '<img class="thumb" src="../images/photo/' + image + '" alt="' + image + '" title="' + image +  '" />'
        + '<span class="insert" id="photo_insert_' + image + '">本文に挿入</span>'
        + '<span class="remove" id="photo_remove_' + id + '_' + number + '">削除</span>'
        + '</li>'
        ;
      });
      jQuery('#photo_list').html(contents);
    },
    upload: function() {
      jQuery('#photo_number').val(that.maxImageNumber);
      jQuery('#photo_id').val(that.editId);
      jQuery('#photo_form').submit();
    },
    afterUpload: function(id, number) {
      var dateObj = new Date;
      var elemId = 'photo_id_' + id + '_' + number;
      var image = id + '_' + number + '.jpg';
      var contents = '<li id="' + elemId + '" style="display: none; opacity: 0;">'
      + '<img class="thumb" src="../images/photo/' + image + '?' + dateObj.getTime() + '" alt="' + image + '" />'
      + '<span class="insert" id="photo_insert_' + image + '">本文に挿入</span>'
      + '<span class="remove" id="photo_remove_' + id + '_' + number + '">削除</span>'
      + '</li>'
      ;
      jQuery(contents).prependTo('#photo_list');
      jQuery('#' + elemId).slideDown('slow', function() { jQuery('#' + elemId).animate({ opacity: 1 }, 'slow'); });
    },
    exTextarea: function() {
      jQuery.fn.extend({
        insertAtCaret: function(v) {
          var o = this.get(0);
          o.focus();
          if (jQuery.browser.msie) {
            var r = document.selection.createRange();
            r.text = v;
            r.select();
          }
          else {
            var s = o.value;
            var p = o.selectionStart;
            var np = p + v.length;
            o.value = s.substr(0, p) + v + s.substr(p);
            o.setSelectionRange(np, np);
          }
        }
      });
    }
  };
  return that;
};

var blogObj = new blog();
jQuery(document).ready(function() {
  jQuery('#rows').bind('click', blogObj.rowsEvent);
  jQuery('#photo_list').bind('click', blogObj.photoListEvent);
  jQuery('#remove_cancel').bind('click', blogObj.cancelRemove);
  jQuery('#remove_ok').bind('click', blogObj.remove);
  jQuery('#edit_ok').bind('click', blogObj.update);
  jQuery('#edit_cancel').bind('click', function() {
    blogObj.editId = '0';
    jQuery('#edit_block').fadeOut();
  });
  jQuery('#add').bind('click', blogObj.add);
  jQuery('#date').datepicker(jQuery.extend({}, jQuery.datepicker.regional.ja, { dateFormat: 'yy-mm-dd' }));
  jQuery('#photo_add').bind('change', blogObj.upload);
  blogObj.exTextarea();
  blogObj.getRows();
});
