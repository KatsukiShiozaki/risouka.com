
//ブラウザ最大化
/*
window.onload = function(){
	w = screen.availWidth;
	h = screen.availHeight;
	moveTo(0,0);
	window.resizeTo(w,h);
}

*/
//ヘッダ画像置き換え
$(function(){

    $("#navi ul li a img[src*='-on']").addClass("current");
  
    $("#navi ul li a img,input[type='image']").hover(function(){
        if ($(this).attr("src")){
            $(this).attr("src",$(this).attr("src").replace("-off.", "-on."));
        }
    },function(){
        if ($(this).attr("src") && !$(this).hasClass("current") ){
            $(this).attr("src",$(this).attr("src").replace("-on.", "-off."));
        }
    });
});





//マウスオーバーで半透明
$(document).ready(
  function(){
    $("a img") .hover(function(){
       $(this).fadeTo("4000",0.5);
    },function(){
       $(this).fadeTo("4000",1.0); 
    });
  });
  
  
  

// Body IDに合わせてnaviにクラス追加
$(function(){
var id = $("body").attr("id");
$("#navi ul li."+id).addClass("selected");
});

  
//スクロール
jQuery.easing.quart = function (x, t, b, c, d) {
    return -c * ((t=t/d-1)*t*t*t - 1) + b;
};
$(function(){
   $('a[href^=#]').click(function() {
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, 1000, 'quart');
      return false;
   });
});


//Drop Down Menu	
$(function(){
	$('#navi ol li:last-child').addClass('last');
	$('#navi ul li').hover(function(){
		$("> ol:not(:animated)" , this).animate({
			height : "toggle",
			opacity : "toggle"
		}, 200 );
	},
	function(){
		$("> ol" , this).fadeOut("fast");
	});
});


/*TOP実績　奇数にクラス*/
$(function(){
	$("#top_example").each(function(){
	$(this).find("div.sub:even").addClass("left");
	});
});

//フォームインプット
$(function(){
	$('#contact input[type=text],#contact textarea').focus(function(){
		$(this).addClass('focus');
	}).blur(function(){
		$(this).removeClass('focus');
	});
});


