<?php include_once("../include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>最新情報|<?php echo($site_name ) ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="../css/import.css" rel="stylesheet" type="text/css">
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



<link href="../css/information.css" rel="stylesheet" type="text/css">


    <script type="text/javascript" language="javascript">
      function ShowNowYear() {
        var now = new Date();
        var year = now.getFullYear();
        document.write(year);
      }
    </script>   
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="../ci/js/information.js"></script>

</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("../tpl_header.php"); ?>
</header>
    





<article id="content">
<h2><img src="../images/title-information.png" alt="最新情報"></h2>

<div id="main">
    
<!--<p class="line1"><img src="../images/common/line1-top.png" alt=""></p>
<p class="line1"><img src="../images/common/line1-btm.png" alt=""></p>-->


          <div id="sub_main">
            <div id="pager"></div>
            <div id="main_box">
              <div class="information">
                <h4>記事タイトル</h4>
              </div>
              <!-- information -->
              <p class="page_top"><a href="#head"><img src="../images/common/btn_page_top.jpg" alt="このページのTOPへ"/></a></p>
            </div>
            <!-- main_box -->
          </div>
          <!-- sub_main -->



            <div id="blog_menu"></div>


</div><!-- main -->

<!--   
<div id="sidebar">


</div>-->
   

</article><!-- content -->




<footer>
<?php require("../tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>