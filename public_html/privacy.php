<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>プライバシーポリシー｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->




</head>
<body>




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">

<div id="main">

<div id="lesson">
<h2><img src="images/title-privacy.png" alt="プライバシーポリシー"></h2>






<section>

  <h3>個人情報保護方針について</h3>
  <div>
    <p>理創花(以下、「当店」といいます。) ではお客様の個人情報保護に全力で取り組んでいます。<br>
      当店では、個人情報を下記の目的に利用し、その取扱には細心の注意を払っています。<br>
      個人情報の取り扱いについてお気づきの点は、お気軽にお問合せ下さい。</p>
  </div>
  <h3>個人情報の収集について</h3>
  <div>
    <p>本ウェブサイトの一部のページでは、以下の場合お客様に個人情報のご提供をお願いする場合があります。<br>
      ・サービスに対するお客さまのご要望やご意見を直接伺うため<br>
      ・通常のメール送信（ご連絡）</p>
  </div>
  <h3>個人情報の保護と管理について</h3>
  <div>
    <p>提供いただく個人情報は、収集する際にお伝えした利用範囲内において、当店の今後サービスの向上のために利用いたします。</p>
  </div>
  <h3>個人情報の保護と管理について</h3>
  <div>
    <p>当店では、個人情報の紛失、誤用、改ざん等を防止する為に、保管・管理には、最善の注意をはらいお取り扱いをさせて頂いております。</p>
    <p>しかし、お客さまの情報送信時およびアクセス過程でのセキュリティは、インターネットの特性上、当店で保証することができません。個人情報を送信する場合は、ご自身の責任において情報を送信していただくようお願いいたします。</p>
    <p>また、このウェブサイトからリンクされている第三者のウェブサイトで収集された個人情報に対しての保護や活用について当店は、いかなる義務や責任を負いかねますのであらかじめご了承ください。</p>
  </div>
  <h3>個人情報の第三者への開示について</h3>
  <div>
    <p>本ウェブサイトでは、送信していただいたお客様の個人情報を、原則としてご本人の同意なく他の会社または個人に開示いたしません。</p>
    <p>ただし、本ウェブサイトを利用されたお客様が第三者に不利益をもたらし、その者から請求があった場合、および裁判所、警察等の公的機関より個人情報の開示を求められた場合等には、これに応じてお客様の個人情報を開示することがあります。</p>
  </div>
</section>



<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->


</div><!-- lesson -->
</div><!-- main -->

    

</article><!-- content -->


<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>