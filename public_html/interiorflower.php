<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>インテリアフラワー｜
		<?= $site_name ?>
	</title>
	<meta name="Keywords" content="">
	<meta name="Description" content="">
	<meta http-equiv="content-script-type" content="text/javascript">
	<meta http-equiv="content-style-type" content="text/css">
	<link href="css/import.css" rel="stylesheet" type="text/css">
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/common.js" type="text/javascript"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



	<!--colorbox-->
	<link href="css/colorbox.css" rel="stylesheet" type="text/css" charset="UTF-8"/>
	<script src="js/jquery.colorbox.js"></script>
	<script type="text/javascript">
		$( function () {
			$( '.colorbox a' ).colorbox( {
				rel: 'group'
			} );
		} );
	</script>

</head>

<body class="home">




	<div id="bg">
		<div id="bg_sub">
			<div id="container">



				<header>
					<?php require("tpl_header.php"); ?>
				</header>






				<article id="content">

					<div id="main">
						<div id="preserved">

							<h2><img src="images/title-interior.png" alt="オーダーメイドプリザーブドフラワー"></h2>


							<section>

								<p class="mb10"><img src="images/interior/catch.jpg" alt="">
								</p>


								<div class="txt txt_catch">
									<p>インテリアフラワーは、室内を彩るインテリアとして、ずっと飾ることのできるお花です。</p>
									<p>自分のお部屋やお店に飾って鮮やかにしたい、お花を贈った相手に長く気に入ってもらいたい・・・<br> そんなあなたに届けたいフラワーメッセージ。予算とご希望をお聞かせください。
									</p>
									<p>永遠に喜んでもらえるお花を、大切な人へ贈ってみてはいかがですか。</p>
								</div>
							</section>


							<section>
								<h3>ギャラリー</h3>

								<div class="txt">
									<p>
										相手にお花を贈りたいけど、どんなお花をプレゼントしたらいいか分からない・・・といった悩みをお持ちの方は ぜひ参考にご覧ください。
									</p>
								</div>


								<div class="gallery_box colorbox interior_box">

									<ul class="clrfix">

										<li><a href="images/interior/1.jpg"><img src="images/interior/1_thum.jpg" alt="製作事例01" ></a> 【注文番号】
											<br>IF-1</li>
										<li><a href="images/interior/2.jpg"><img src="images/interior/2_thum.jpg" alt="製作事例01" ></a> 【注文番号】
											<br>IF-2</li>
										<li><a href="images/interior/3.jpg"><img src="images/interior/3_thum.jpg" alt="製作事例01" ></a> 【注文番号】
											<br>IF-3</li>
									</ul>

								</div>
								<!-- gallery_box -->
								
								<div class="order_area">
									<p> ご注文の際は、ご希望のお花の【注文番号】をお控えのうえ、<br>下記の「注文する」ボタンを押した後、『お問い合わせ内容』の箇所に、<br>【注文番号】と【数量】と【予算】を記載のうえ、お問い合わせください。</p>
									<div class="btn_order"><a href="contact/">注文する</a></div>
								</div>

							</section>


							<!--<section id="flow">

<h3>ご注文から完成の流れ<span>　お電話でのご相談もご遠慮なくお申し付け下さい。 </span></h3>


<div class="flow_chart">

<table>
	<tr>
		<th><h4>ご来店</h4></th>
        <td><p>またはお電話でのご相談も承ります。</p>
</td>
	</tr>
	<tr>
	  <td class="arrow" colspan="2"><img src="images/common/arrow.png" alt="↓"></th>
	  </tr>
	<tr>
		<th><h4>ご注文</h4></th>
        <td><p>お客様のご要望を詳しくヒアリングさせていただきご注文いただきます。</p>
</td>
	</tr>
	<tr>
	  <td class="arrow" colspan="2"><img src="images/common/arrow.png" alt="↓"></th>
	  </tr>
	<tr>
		<th><h4>製作作業</h4></th>
        <td><p>ヒアリング内容を基に製作させていただきます。製作内容により納期は多少前後します。</p>
</td>
	</tr>
	<tr>
	  <td class="arrow" colspan="2"><img src="images/common/arrow.png" alt="↓"></th>
	  </tr>
	<tr>
		<th><h4>商品受取orお届け</h4></th>
        <td><p>商品の受取はご来店または発送も承っております。<br>
        発送の場合は別途送料がかかります。</p>

</td>
</tr>

</table>





</div>


</section>-->


							<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->




						</div>
						<!-- lesson -->
					</div>
					<!-- main -->



				</article>
				<!-- content -->




				<footer>
					<?php require("tpl_footer.php"); ?>
				</footer>

			</div>
			<!-- container -->
		</div>
		<!-- bg_sub -->
	</div>
	<!-- bg -->



</body>
</html>