var topPicture = function() {
  var that = {
    update: function() {
      jQuery.ajax({
        url: './top_picture/update',
        type: 'POST',
        data: { message: jQuery('#message').val() },
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            jQuery('#update_message').html('更新しました。');
            jQuery('#update_message').animate({ opacity: 1 }, 'slow');
            setTimeout(that.fadeOutMsg, 3000);
          }
        }
      });
    },
    fadeOutMsg: function () {
      jQuery('#update_message').animate({ opacity: 0 }, 'slow');
    },
    upload: function() {
      jQuery('#photo_number').val(that.maxImageNumber);
      jQuery('#photo_id').val(that.editId);
      jQuery('#photo_form').submit();
    },
    afterUpload: function(id, number) {
      var dateObj = new Date;
      var contents = '<img src="../images/top_picture/top_picture_tmp.jpg?' + dateObj.getTime() + '" alt="" />';
      var photoElem = jQuery('#photo_display');
      photoElem.fadeOut('slow', function() {
        photoElem.html(contents);
        photoElem.fadeIn('slow');
      });
    }
  };
  return that;
};

var topPictureObj = new topPicture();
jQuery(document).ready(function() {
  jQuery('#photo').bind('change', topPictureObj.upload);
  jQuery('#ok_button').bind('click', topPictureObj.update);
});
