var topPage = function() {
  var that = {
    rows: [],
    message: '',
    reNL: /\r?\n/g,
    getRows: function() {
      jQuery.ajax({
        url: './ci/information/get_for_top/',
        type: 'POST',
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.rows         = json.rows;
            that.setRows();
          }
        }
      });
    },
    setRows: function() {
      var contents = '';
      jQuery.each(that.rows, function(i, row) {
        var date = row.date.replace(/-/g, '/');
        contents += ''
        + '<p class="n_day">' + date + '</p>'
        + '<p class="n_txt"><a href="./information/?news_' + row.id + '">' + row.title + '</a></p>'
        ;
      });
      jQuery('#news').html(contents);
    },
    getTopPicture: function() {
      jQuery.ajax({
        url: './ci/top/get_picture',
        type: 'POST',
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.message = json.message;
            that.setTopPicture();
          }
        }
      });
    },
    setTopPicture: function() {
      var contents = '<div><img class="top_picture" src="./ci/images/top_picture/top_picture.jpg" alt="" width="160" /></div>'
      + '<p style="text-align: left; margin: 1em;">' + that.message.replace(that.reNL, '<br />') + '</p>'
      ;
      jQuery('#left_box').css({ height: 'auto' });
      jQuery('#top_picture').html(contents);
      setTimeout(function() {
        var height = jQuery('#left_box').height();
        jQuery('#center_box').animate({ height: height });
        jQuery('#right_box').animate({ height: height });
      }, 500);
    }
  };
  return that;
};

var topObj = new topPage();
jQuery(document).ready(function() {
  topObj.getRows();
  topObj.getTopPicture();
});
