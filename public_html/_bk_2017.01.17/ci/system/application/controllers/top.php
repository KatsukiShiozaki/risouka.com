<?php
class Top extends Controller {
    function Top() {
        parent::Controller();
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
    }

    function index() {
        $rows = $this->db->get('information')->result_array();
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'rows' => $rows));
    }

    function get_picture() {
        $row = $this->db->get('top_picture')->row_array();
        $result = 'no-picture';
        if (file_exists('./images/top_picture/top_picture.jpg')) {
            $result = 'okay';
        }
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => $result, 'message' => $row['message']));
    }
}
?>
