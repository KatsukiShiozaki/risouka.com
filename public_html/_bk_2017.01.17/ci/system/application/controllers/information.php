<?php
class Information extends Controller {
    function Information(){
        parent::Controller();
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
    }

    function index() {
    }

    function get($page = 1, $category = 0, $archive = '') {
        $rows = $this->db->get('blog')->result_array();
        $year_month_list = array();
        foreach ($rows as $row) {
            $year_month = preg_replace('/^(\d{4})-(\d{2}).*/', '$1_$2', $row['date']);
            $year_month_list[] = $year_month;
        }
        $year_month_list = array_values(array_unique($year_month_list));
        if ($category) {
            $this->db->where('category_id', $category);
        }
        if ($archive) {
            $this->db->like('date', preg_replace('/_/', '-', $archive));
        }
        $count = $this->db->count_all_results('blog');

        $offset = ($page - 1) * 5;
        if ($category) {
            $this->db->where('category_id', $category);
        }
        if ($archive) {
            $this->db->like('date', preg_replace('/_/', '-', $archive));
        }
        $this->db->limit(5, $offset);
        $this->db->order_by('date', 'DESC');
        $this->db->order_by('update_time', 'DESC');
        $rows = $this->db->get('blog')->result_array();
        $category_rows = $this->db->get('category')->result_array();

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array(
                'result'          => 'okay',
                'rows'            => $rows,
                'count'           => $count,
                'category_rows'   => $category_rows,
                'year_month_list' => $year_month_list
            ));
    }

    function get_for_top() {
        $this->db->order_by('date', 'DESC');
        $this->db->order_by('update_time', 'DESC');
        $rows = $this->db->get('blog')->result_array();

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array(
                'result'          => 'okay',
                'rows'            => $rows,
            ));
    }
}
?>
