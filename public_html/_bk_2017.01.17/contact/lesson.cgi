#!/usr/bin/perl

#jcode.pl のパス（相対パス指定）
$jcode = './jcode.pl';

#sendmail のパス（要変更）
$sendmail = '/usr/sbin/sendmail';

#一時保存用ファイルのパス（相対パス指定）
$tempfile = './tempfile.dat';

#メールを受け取るアドレス
#$getmail = 'lesson@risouka.com';
$getmail = 'info@risouka.com';


#******************************************************************************

#送信完了画面のパス（http://〜の絶対パス指定）
$complete = 'http://www.risouka.com/contact/end2.html';

#送信確認用テンプレートのパス（相対パス指定）
$checktemplete = './check2.html';

#エラー表示用テンプレートのパス（相対パス指定）
$errortemplete = './error2.html';

#******************************************************************************

#メールのタイトル
# 入力させる場合には、値を設定しないでください。	
$titleinput = 'レッスンコースお申し込みフォーム';

#メールのサブタイトル
# メールフォームから発信されたことを示すために、タイトルに追加されます。
$subtitle = 'ホームページより';

#名前の入力を必須にするなら 1 を設定。しないなら 0 を。
$nameinput = 1;

#メールアドレスの入力を必須にするなら 1 を設定。しないなら 0 を。
# コピーメールを送信する場合は、自動的に必須になります。
$mailinput = 1;

#電話番号の入力を必須にするなら 1 を設定。しないなら 0 を。
$telinput = 1;

#FAXの入力を必須にするなら 1 を設定。しないなら 0 を。
$faxinput = 0;

#フリガナの入力を必須にするなら 1 を設定。しないなら 0 を。
$name2input = 1;

#郵便番号1の入力を必須にするなら 1 を設定。しないなら 0 を。
$ad1input = 1;

#郵便番号2の入力を必須にするなら 1 を設定。しないなら 0 を。
$ad2input = 1;

#都道府県の入力を必須にするなら 1 を設定。しないなら 0 を。
$ad3input = 1;

#市区町村の入力を必須にするなら 1 を設定。しないなら 0 を。
$ad4input = 1;

#レッスンコースの入力を必須にするなら 1 を設定。しないなら 0 を。
$lessoninput = 1;



#******************************************************************************

#表示デリミタ
# 自由設定の入力項目で、複数選択された時の、
# Web上で表示するデータの区切り記号を設定します。
$indidelimita = "<br>";

#メールデリミタ
# 表示デリミタと同じですが、こちらはメールの文章内に使用するものです。
# 改行を設定したい時は \n と設定してください。
$maildelimita = "/";

#******************************************************************************

#送信確認を行うなら 1 を設定。しないなら 0 を設定。
$mailcheck = 1;

#******************************************************************************

#コピーメールを送信するなら 1 を設定。しないかユーザーに選択させるなら 0 を。
$copymail = 1;

#コピーメール送信有無の表示
# 送信確認時に表示するものです。
$copymailname2[1]= 'Receive';#コピーメールを受け取る時
$copymailname2[0]= 'Deny';#受け取らないとき

#コピーメールであることを示すメッセージ
# ここで設定した文章が、コピーメールの最上部に表示されます。

$copymailsig = <<'EOD';
------------------------------------------------------------
理創花
ホームページより
レッスンコースのお申し込みをいただき
誠にありがとうございます。



理創花
URL：http://www.risouka.com/
E-Mail：info@risouka.com
------------------------------------------------------------
EOD

#コピーメールの送信者名
$copymailfrom = 'info@risouka.com';


#******************************************************************************

#投稿を禁止するワードを設定
# ここで設定した文字がメッセージ内に入っているメールは送信できなくなります。
# 例：@denyword = ('死','殺');
@denyword = ('死','殺');

#投稿を禁止するホスト名/IP
# ここで指定した文字を含むホスト名またはIPからの投稿は出来なくなります。
# 例：@denyhost = ('','');
@denyhost = ();

#同一IPからの連続投稿を禁止するなら 1 を。しないなら 0 を設定。
$denydouble = 0;

#******************************************************************************

#他サイトからの投稿を禁止するなら 1 を設定。
# 1 にすると、以下で指定する2つのアドレス以外からは投稿できなくなります。
# サーバによってはうまく動かない場合もあります。
# 投稿しようとした場合、入力フォームのアドレスに飛びます。
$denyother = 0;

#入力フォームのアドレス(http://〜の絶対パス)
$enterformurl = 'http://www.risouka.com/contact/index.html';

#このCGIのアドレス(http://〜の絶対パス)
$ownurl = 'http://www.risouka.com/contact/lesson.cgi';

#******************************************************************************
#METAタグを使用してページジャンプを行う場合は 1 を。しないなら 0 を設定。
# 基本的には 0 のまま使用します。
# 送信完了画面がうまく表示されない場合のみ 1 を設定してください。
$metajump = 0;

#******************************************************************************

require $jcode;

$cl = $ENV{"CONTENT_LENGTH"};
if( $cl > 0 ){
	read(STDIN, $qs, $cl );
}else{
	$qs = $ENV{"QUERY_STRING"};
}

@contents = split(/&/,$qs);
foreach $i (0 .. $#contents) {
	local($key,$text)= split(/=/,$contents[$i]);
	$text =~ s/\+/ /g;
	$text =~ s/%(..)/pack("c",hex($1))/ge;
	$text =~ s/\r\n/\n/g; 
	$text =~ s/\r/\n/g; 
	&jcode'convert(*text,"sjis"); 
	$act = $text if $key eq 'act';
	$name = $text if $key eq 'name';
	$mail = $text if $key eq 'mail';
	$mail2 = $text if $key eq 'mail2';
	$ad1 = $text if $key eq 'ad1';
	$ad2 = $text if $key eq 'ad2';
	$ad3 = $text if $key eq 'ad3';
	$ad4 = $text if $key eq 'ad4';
    $tel = $text if $key eq 'tel';
    $fax = $text if $key eq 'fax';
    $title = $text if $key eq 'title';
	$name2 = $text if $key eq 'name2';
	$lesson = $text if $key eq 'lesson';
    $copymailcheck = $text if $key eq 'copymailcheck';
	$datatitle[$1] = $text if $key =~ /^datatitle(\d+)/i;
	$datacheck[$1] = $text if $key =~ /^datacheck(\d+)/i;
	if ($key =~ /^data(\d+)/i){
		$data[$1] .= "<>" if $data[$1] ne "";
		$data[$1] .= $text;
		}
}

$copymailcheck = 0 if $copymailcheck != 1;

$ip = $ENV{'REMOTE_ADDR'};
$host = gethostbyaddr(pack("C4", split(/\./, $ip)), 2);
$host = $ENV{'REMOTE_HOST'} if $host eq "";
$host = $ip if $host eq "";
$useragent = $ENV{'HTTP_USER_AGENT'};
if (($mailcheck == 0) && ($act eq "check")){$act = "send";}

if (($act eq "") || (($denyother) && (($ENV{'HTTP_REFERER'} !~ /$enterformurl/i) && ($ENV{'HTTP_REFERER'} !~ /$ownurl/)))){
	&jump($enterformurl);
	}
elsif ($act eq "check"){&check;}
elsif ($act eq "send"){&send;}
exit;

#*****************************************************************************
sub datacheck{
#入力データチェック
while(1){last if (!(chomp($name2)))}
if ($titleinput ne ""){$title = $titleinput;}
elsif ($title eq ""){&error("件名を入力してください。");}
if (($lessoninput) && ($lesson eq "")){&error("レッスンコースを選択してください。");}
if (($nameinput) && ($name eq "")){&error("お名前を入力してください。");}
if (($name2input) && ($name2 eq "")){&error("フリガナを入力してください。");}
if (($telinput) && ($tel eq "")){&error("お電話番号を入力してください。");}
if (($ad1input) && ($ad1 eq "")){&error("郵便番号を入力してください。");}
if (($ad2input) && ($ad2 eq "")){&error("郵便番号を入力してください。");}
if (($ad3input) && ($ad3 eq "")){&error("都道府県を入力してください。");}
if (($ad4input) && ($ad4 eq "")){&error("市区町村を入力してください。");}
if ((($mailinput) || ($copymail+$copymailcheck)) && ($mail eq "")){
	&error("メールアドレスを入力してください。");
	}
if (($mail ne "") && ($mail !~ /[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/)){&error("メールアドレスが間違っています。");}
if ((($mailinput2) || ($copymail+$copymailcheck)) && ($mail2 eq "")){
	&error("確認用メールアドレスを入力してください。");
	}
if (($mail2 ne "") && ($mail2 !~ /[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/)){&error("確認用メールアドレスが間違っています。");}
if ($mail ne $mail2){
	&error("メールアドレスと確認用メールアドレスが不一致です。");
}
if ($denyword[0]){foreach(@denyword){&error("このメッセージは送信できません。") if index($name2,$_) != -1;}}
if ($denyhost[0]){foreach(@denyhost){&error("あなたの接続元からは送信できません。") if (index($host,$_) != -1) || (index($ip,$_) != -1);}}


for(1 .. 99){
	last if $datatitle[$_] eq "";
	&error("$datatitle[$_]のデータを入力してください。") if ($datacheck[$_]) && ($data[$_] eq "");
	}
}
#*****************************************************************************
sub send{
&datacheck;
my ($temp,$mailbody,$mailbody2,$mailhead);

if ($denydouble){
	open (IO,"+<$tempfile");
	eval{flock(IO,2)};
	$temp=<IO>;
	if ($temp eq $ip){
		close(IO);
		&error("連続投稿は禁止されています。");
		}else{
		truncate(IO,0);
		seek(IO,0,0);
		print IO $ip;
		close(IO);
		}
	}

$mailhead .= "Subject: $title($subtitle)\n";
$mailhead .="Content-Transfer-Encoding: 7bit\n";
$mailhead .="Content-Type: text/plain; charset=ISO-2022-JP\n";
$mailhead .="X-Mailer: Mail Form by The Room\n\n";

$mailbody .= "お申し込みレッスンコース：$lesson\n" if $lesson ne "";
$mailbody .= "お名前：$name\n" if $name ne "";
$mailbody .= "フリガナ：$name2\n" if $name2 ne "";
$mailbody .= "電話番号：$tel\n" if $tel ne "";
$mailbody .= "FAX番号：$fax\n" if $fax ne "";
$mailbody .= "〒：$ad1" if $ad1 ne "";
$mailbody .= "-$ad2\n" if $ad2 ne "";
$mailbody .= "住所：$ad3" if $ad3 ne "";
$mailbody .= "$ad4\n" if $ad4 ne "";	
$mailbody .= "メールアドレス：$mail\n" if $mail ne "";
$mailbody .= "\n" if ($title ne "") || ($name2 ne "");


if ($datatitle[1] ne ""){
	$mailbody .= "\n";
	for (1 .. 99){
		last if $datatitle[$_] eq "";
		$data[$_] =~ s/<>$//i;
		$data[$_] =~ s/<>/$maildelimita/ig;
		$mailbody.="$datatitle[$_]：$data[$_]\n";
		}
	}
$mailbody .= "\n";

$mailbody2 .= <<EOD;
-----------------------------------------------------------
投稿者の情報

IPアドレス：$ip
ホスト名：$host
ユーザーエージェント：$useragent
-----------------------------------------------------------
EOD

$temp = "To: $getmail\nFrom: ";
if ($mail ne ""){$temp.="$mail\n";}else{$temp.= 'from@mail.form'."\n";}
$temp .= $mailhead.$mailbody.$mailbody2."\n";
&jcode::convert(\$temp,'jis');

open(MAIL,"| $sendmail -t") || die &error("sendmailが使用できませんでした。");
print MAIL $temp;
close (MAIL);
sleep(1);

if (($copymailcheck) || ($copymail)){
	$temp = "To: $mail\nFrom: $copymailfrom\n";
	$temp .=$mailhead.$copymailsig.$mailbody;
	&jcode::convert(\$temp,'jis');

	open(MAIL,"| $sendmail -t") || die &error("sendmailが使用できませんでした。");
	print MAIL $temp;
	close (MAIL);
	sleep(1);
	}
&jump($complete);
}
#*****************************************************************************
sub check{
#入力確認画面
&datacheck;

my ($buffer,$buffer2,$temp);
open(IN,"$checktemplete");
while(<IN>){$buffer2.=$_;}
close(IN);
$temp = $name2;
$temp =~ s/\n/$indidelimita/ig;
$buffer2 =~ s/<!--TITLE-->/$title/ig;
$buffer2 =~ s/<!--LESSON-->/$lesson/ig;
$buffer2 =~ s/<!--NAME-->/$name/ig;
$buffer2 =~ s/<!--MAIL-->/$mail/ig;
$buffer2 =~ s/<!--MAIL2-->/$mail2/ig;
$buffer2 =~ s/<!--AD1-->/$ad1/ig;
$buffer2 =~ s/<!--AD2-->/$ad2/ig;
$buffer2 =~ s/<!--AD3-->/$ad3/ig;
$buffer2 =~ s/<!--AD4-->/$ad4/ig;
$buffer2 =~ s/<!--TEL-->/$tel/ig;
$buffer2 =~ s/<!--FAX-->/$fax/ig;
$buffer2 =~ s/<!--NAME2-->/$name2/ig;
$buffer2 =~ s/<!--COPYMAIL-->/$copymailname2[$copymailcheck]/ig;

$buffer = <<EOD;
<input type="hidden" name="title" value="$title">
<input type="hidden" name="lesson" value="$lesson">
<input type="hidden" name="name" value="$name">
<input type="hidden" name="mail" value="$mail">
<input type="hidden" name="mail2" value="$mail2">
<input type="hidden" name="ad1" value="$ad1">
<input type="hidden" name="ad2" value="$ad2">
<input type="hidden" name="ad3" value="$ad3">
<input type="hidden" name="ad4" value="$ad4">
<input type="hidden" name="tel" value="$tel">
<input type="hidden" name="fax" value="$fax">
<input type="hidden" name="name2" value="$name2">
<input type="hidden" name="copymailcheck" value="$copymailcheck">
EOD

for(1 .. 99){
	last if $datatitle[$_] eq "";
	$data[$_] =~ s/<>$//ig;
	$buffer.=<<EOD;
<input type="hidden" name="datatitle$_" value="$datatitle[$_]">
<input type="hidden" name="datacheck$_" value="$datacheck[$_]">
<input type="hidden" name="data$_" value="$data[$_]">
EOD
	$temp = $data[$_];
	$temp =~ s/<>/$indidelimita/ig;
	$buffer2 =~ s/<!--DATA$_-->/$temp/ig;
	}
$buffer2 =~ s/<!--ALLDATA-->/$buffer/ig;

print "Content-type:text/html; charset=shift_jis\n\n".$buffer2;
exit;

}
#*****************************************************************************
sub error{
#エラー表示
print "Content-type:text/html; charset=shift_jis\n\n";
open (IN,"$errortemplete");
while(<IN>){
	$_ =~ s/<!--ERROR-->/$_[0]/ig;
	print;
	}
close(IN);
exit;
}
#*****************************************************************************
sub jump{
#ページジャンプ
if ($metajump == 0){
	print "Location: $_[0]\n\n";
	}else{
print <<EOD;
Content-type:text/html; charset=shift_jis

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=shift_jis">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="refresh" content="0;url=$_[0]">
<title>Waiting...</title>
</head>
<body>
</body>
</html>
EOD
	}
exit;
}
#*****************************************************************************