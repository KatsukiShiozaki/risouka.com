<?php
class Top_picture extends Controller {
    function Top_picture() {
        parent::Controller();
        $this->load->library(array('simplelogin', 'validation', 'session'));
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
        if (!$this->session->userdata('logged_in')) {
            redirect('admin/login', 'refresh');
        }
        $this->file_path = './images/top_picture/';
    }

    function index() {
        if (file_exists('./images/top_picture/top_picture_tmp.jpg')) {
            unlink('./images/top_picture/top_picture_tmp.jpg');
        }
        $top_picture_row = $this->db->get('top_picture')->row_array();
        $data = array(
            'title'    => 'トップ写真 管理',
            'base_url' => base_url(),
            'css_list' => array('admin/top_picture.css'),
            'js_list'  => array('ui.datepicker-ja.js', 'admin/top_picture.js'),
            'message'  => $top_picture_row['message']
        );
        $this->load->view('admin/head.tpl', $data);
        $this->load->view('admin/menu.tpl', $data);
        $this->load->view('admin/top_picture.tpl', $data);
        $this->load->view('admin/foot.tpl');
    }

    function update() {
        $data = array(
            'message' => $this->input->post('message'),
        );
        $this->db->update('top_picture', $data);
        if (file_exists('./images/top_picture/top_picture_tmp.jpg')) {
            rename('./images/top_picture/top_picture_tmp.jpg', './images/top_picture/top_picture.jpg');
        }

        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay'));
    }

    function upload() {
        $id     = $this->input->post('id', true);
        $number = $this->input->post('number', true);
        $config['upload_path']   = $this->file_path;
        $config['allowed_types'] = 'jpg';
        $config['overwrite']     = true;
        $config['file_name']     = 'top_picture_tmp';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo')) {
          $error = array('error' => $this->upload->display_errors('', ''));
          var_dump($error);
          echo '<html>'
              . '<body>'
              . '<script type="text/javascript">'
              . 'alert("' . implode('', $error) . '")'
              . '</script>'
              . '</body>'
              . '</html>'
              ;
        }
        else {
          $data = array('upload_data' => $this->upload->data());
          echo '<html>'
              . '<body>'
              . '<script type="text/javascript">'
              . 'window.top.topPictureObj.afterUpload();'
              . '</script>'
              . '</body>'
              . '</html>'
              ;
        }
        if (file_exists('./images/top_picture/top_picture_tmp.JPG')) {
            rename('./images/top_picture/top_picture_tmp.JPG', './images/top_picture/top_picture_tmp.jpg');
        }
    }
}
?>
