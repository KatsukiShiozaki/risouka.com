<?php
class Information extends Controller {
    function Information() {
        parent::Controller();
        $this->load->helper(array('url', 'form', 'json'));
        $this->load->database();
    }

    function index() {
        $rows = $this->db->get('information')->result_array();
        header('content-type:application/json;charset=utf-8');
        echo json_encode(array('result' => 'okay', 'rows' => $rows));
    }
}
?>
