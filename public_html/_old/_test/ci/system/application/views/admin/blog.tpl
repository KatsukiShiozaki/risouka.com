<script src="http://maps.google.com/maps/api/js?sensor=false&language=ja"></script>
<div class="content">
  <div id="category_block" style="position: relative;">
    <div id="category_select_block">
      <span id="disp_category"><span id="disp_category_name">全てのカテゴリ</span><img id="category_rows_on" src="../images/admin/stock_list_down.png" alt="" /></span>
      <span id="category_add">追加</span>
    </div>
    <ul id="category_rows" style="display: none;">
    </ul>
    <div id="category_edit_block" style="display: none;">
      <input name="name" id="edit_category" type="text" />
      <span id="category_edit_ok">OK</span>
      <span id="category_edit_cancel">cancel</span>
    </div>
  </div>
  <div id="category_remove_confirm" style="display: none;">
    <h4>カテゴリの削除</h4>
    <div id="category_remove_contents">
      <p><span id="category_remove_title"></span>を削除します。</p>
      <div class="buttons">
        <input name="category_remove_cancel" id="category_remove_cancel" type="button" value="Cancel" />
        <input name="category_remove_ok" id="category_remove_ok" type="button" value="OK"/>
      </div>
    </div>
  </div>
  <div class="add">
    <span id="add">記事の追加</span>
  </div>
  <div>
    <span class="rows_head" id="rows_head_date">日付</span>
    <span class="rows_head" id="rows_head_title">タイトル</span>
    <span class="rows_head" id="rows_head_edit">編集</span>
    <span class="rows_head" id="rows_head_remove">削除</span>
  </div>
  <ul id="rows">
  </ul>
  <div class="clear"></div>
  <div id="remove_confirm" style="display: none;">
    <h4>記事の削除</h4>
    <div id="remove_contents">
      <p><span id="remove_title"></span>を削除します。</p>
      <div class="buttons">
        <input name="remove_cancel" id="remove_cancel" type="button" value="Cancel" />
        <input name="remove_ok" id="remove_ok" type="button" value="OK"/>
      </div>
    </div>
  </div>
  <div id="edit_block" style="display: none;">
    <h4>記事の編集</h4>
    <div id="edit_contents">
      <div id="edit_contents_left">
        <form method="post" id="edit_form">
          <div class="inputs">
            <div>
              <label for="date">日付: </label>
              <input name="date" id="date" type="text" readonly="readonly" />
            </div>
            <div>
              <label for="title">タイトル: </label>
              <input name="title" id="title" type="text" />
            </div>
            <div>
              <label for="category_id">カテゴリ: </label>
              <select name="category_id" id="category_id"></select>
            </div>
            <div>
              <label for="text">本文: </label>
              <textarea id="text" type="text" cols="5" rows="5"></textarea>
            </div>
          </div>
          <div class="buttons">
            <input name="edit_cancel" id="edit_cancel" type="button" value="cancel" />
            <input name="edit_ok" id="edit_ok" type="button" value="OK" />
          </div>
        </form>
      </div>
      <div id="edit_contents_right">
        <form id="photo_form" action="blog/upload" target="upload_target" method="post" enctype="multipart/form-data">
          <input name="photo" id="photo_add" type="file" title="写真は「JEPG」のみ登録できます。" accept="image/jpeg" />
          <input name="number" id="photo_number" type="hidden" value="1" />
          <input name="id" id="photo_id" type="hidden" value="0" />
        </form>
        <ul id="photo_list">
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="calContainer" style="display: none;"></div>
<iframe id="upload_target" name="upload_target" src="" style="width:0; height:0; border: 0px solid #fff;"></iframe>
