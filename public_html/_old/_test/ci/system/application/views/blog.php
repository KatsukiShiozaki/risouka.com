<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xml:lang="ja" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="ROBOTS" content="ALL" />
<title>特優賃・高優賃｜株式会社ミサワハウス</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="prev" content="" />
<meta name="Author" content="" />
<meta name="copyright" content="著作権" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="imagetoolbar" content="no" />
<link href="../css/default.css" rel="stylesheet" type="text/css" />
<link href="../css/auxiliary.css" rel="stylesheet" type="text/css" />
<link href="./css/pager.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/onmouse.js"></script>
<script type="text/javascript" src="../js/scroll.js"></script>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/pager.js"></script>
<script type="text/javascript" src="./js/auxiliary.js"></script>
</head>

<body>
<div id="bg">
<div id="container">
<div id="sub_top_bg">
<h2 id="logo"><a href="../"><img src="../images/common/logo.jpg" alt="豊かな郷土にゆとりのわが家　株式会社ミサワハウス" width="368" height="75" /></a></h2>
<div id="head_right">
<ul class="head_link">
  <li><a href="../privacy">プライバシーポリシー</a></li>
  <li><a href="../sitemap">サイトマップ</a></li>
</ul>
<p class="clr_sps"></p>
<p class="bnr"><img src="../images/common/ad.jpg" alt="〒791-8022愛媛県松山市美沢2丁目5-62　TEL:089-925-7632" width="532" height="40" /></p>
</div>
<!-- head_right -->

<div id="flash">
<img src="../images/common/sub_image.jpg" alt="イメージ" width="956" height="164" />
</div>
<!-- flash -->

<div id="head_menu">
<ul>
  <li><a href="../about_us" onmouseover="change_image('img2',104)" onmouseout="change_image('img2',103)">
  <img src="../images/head_menu/btn_about-us.gif" alt="会社概要" width="133" height="60" id="img2" name="img2" /></a></li>
  <li><a href="../contact" onmouseover="change_image('img5',110)" onmouseout="change_image('img5',109)">
  <img src="../images/head_menu/btn_contact.gif" alt="お問い合わせ" width="134" height="60" id="img5" name="img5" /></a></li>
  <li><a href="../request" onmouseover="change_image('img4',108)" onmouseout="change_image('img4',107)">
  <img src="../images/head_menu/btn_request.gif" alt="パンフレット請求" width="134" height="60" id="img4" name="img4" /></a></li>
  <li><a href="../" onmouseover="change_image('img1',102)" onmouseout="change_image('img1',101)">
  <img src="../images/head_menu/btn_home.gif" alt="HOME" width="133" height="60" id="img1" name="img1" /></a></li>    
</ul>
</div>
<!-- head_menu -->
</div>
<!-- sub_top_bg -->

<div id="contents">
<div id="sub_main">
<h3><img src="../images/auxiliary/title.jpg" alt="特優賃・高優賃" width="701" height="28" /></h3>
<p class="p_l"><a href="../">HOME</a>&nbsp;&raquo;&nbsp;特優賃・高優賃</p>

<div id="auxiliary">

<div id="auxiliary_box02">
<h4><img src="../images/auxiliary/title_tyc.jpg" alt="特優賃とは？" width="204" height="43" /></h4>
<h5>■特優賃（特定優良賃貸住宅）とは？</h5>
<p>特優賃とは平成5年に国によって定められた「特定優良賃宅住宅供給促進制度」を活用して出来た中堅所得者向けの賃貸住宅です。<br />
当社の物件では<b>ジュエルミサワ・11</b>がこれに該当します。</p>
<h6>ポイント①　住宅の質が高い</h6>
<p class="point">特優賃と認定される為に、松山市の認定基準をクリアする必要があります。<br /></p>
<ul>
  <li>→マンションの質が高い！<br />（松山市において、特優賃に認定されている物件は、わずか4件です。）</li>
</ul>
<h6>ポイント②　国・自治体より家賃補助が出る</h6>
<p class="point">定期賃貸借契約で定める家賃（契約家賃）から、松山市の補助を差し引いた額を入居者負担額としてお支払いいただきます。<br />
（松山市の補助額は入居者様の所得等によって変動します。）<br /></p>
<ul>
  <li>→ジュエルミサワ・11では家賃補助は終了しました。</li>
</ul>
<h6>ポイント③　仲介手数料なし</h6>
<p class="point">自社物件ですので、仲介手数料など、余分な費用はありません。</p>
</div>
<!-- auxiliary_box02 -->

<div id="auxiliary_box01">
<h4><img src="../images/auxiliary/title_kyc.jpg" alt="高優賃とは？" width="204" height="43" /></h4>
<h5>■高優賃（高齢者向け優良賃貸住宅）とは？</h5>
<p>「高齢者の居住の安定確保に関する法律」に基づき整備された、高齢者の身体機能に対応した設計、設備など、高齢者に配慮した賃貸住宅です。<br />
当社の物件では、<b>ジュエルミサワ・15</b>がこれに該当します。</p>
<h6>ポイント①　高齢者向けに良好な居住環境を備えている</h6>
<p class="point">高優賃と認定される為に、松山市の認定基準をクリアする必要があります。<br /></p>
<ul>
  <ul>
    <li>→高齢者が安心して居住できるように「バリアフリー化」されている。<br />
      「緊急時対応サービス」の利用が可能です。</li>
  </ul>
</ul>
<h6>ポイント②　国・自治体より家賃補助が出る</h6>
<p class="point">定期賃貸借契約で定める家賃（契約家賃）から、松山市の補助を差し引いた額をお支払いいただきます。<br />
（所得によっては補助が出ない場合があります。）</p>
<h6>ポイント③　仲介手数料なし</h6>
<p class="point">自社物件ですので、仲介手数料など、余分な費用はありません。</p>
</div>
<!-- auxiliary_box01 -->

<div class="clr_sps">&nbsp;</div>

<h4><img src="../images/auxiliary/s_title01.jpg" alt="対応マンション" width="702" height="26" /></h4>
<div class="support_box">
<h5><img src="../images/auxiliary/support_title01.jpg" alt="高優賃対応マンション　ジュエルミサワ11" width="311" height="20" /></h5>
<p class="pic"><img src="../images/auxiliary/support_pic01.jpg" alt="ジュエルミサワ11" width="110" height="110" /></p>
<p class="txt">【所在地】 愛媛県松山市森松町725-7<br />
【建物構造】 鉄筋コンクリート造7階建<br />
【総戸数】 12戸（3LDK）<br />
【駐車場】 16台<br />
（1ヶ月5,000円/14台・3,000円/2台）</p>
<p class="clr_sps">&nbsp;</p>
</div>
<!-- support_box01 -->

<div class="support_box">
<h5><img src="../images/auxiliary/support_title02.jpg" alt="特優賃対応マンション　ジュエルミサワ15" width="311" height="20" /></h5>
<p class="pic"><img src="../images/auxiliary/support_pic02.jpg" alt="ジュエルミサワ15" width="110" height="110" /></p>
<p class="txt">【所在地】 愛媛県松山市森松町725-7<br />
【建物構造】 鉄筋コンクリート造7階建<br />
【総戸数】 12戸（3LDK）<br />
【駐車場】 16台（1ヶ月5,000円）<br />
<span class="t_red">平成23年3月完成予定</span></p>
<p class="clr_sps">&nbsp;</p>
</div>
<!-- support_box02 -->

<div class="clr_sps">&nbsp;</div>

<h4><img src="../images/auxiliary/s_title02.jpg" alt="空室一覧" width="702" height="26" /></h4>
<div id="availability_tb_pager" class="pager"></div>

<table class="title_tb" cellspacing="4" cellpadding="0" summary="空室一覧項目名">
  <tr>
    <td class="td01"><span class="t_red3">■</span>物件名</td>
    <td class="td02"><span class="t_red3">■</span>号室</td>
	<td class="td03"><span class="t_red3">■</span>間取り</td>
    <td class="td04"><span class="t_red3">■</span>家賃（円）</td>
    <td class="td05"><span class="t_red3">■</span>共益費（円）</td>
    <td class="td06"><span class="t_red3">■</span>敷金</td>
    <td class="td07"><span class="t_red3">■</span>状況</td>
    <td class="td08">&nbsp;</td>
  </tr>
</table>

<?php foreach($rows as $row): ?>
<table class="availability_tb" cellspacing="5" cellpadding="0" summary="空室一覧">
  <tr>
    <td class="td01"><img src="../images/common/head_arrow02.gif" alt="" width="9" height="9" />&nbsp;<?php echo $row['building_name']; ?></td>
    <td class="td02"><a href="./details/rental/<?php echo $row['id']; ?>"><?php echo $row['number']; ?></a></td>
    <td class="td03"><?php echo $row['layout']; ?></td>
    <td class="td04"><?php echo $row['price_standard']; ?></td>
    <td class="td05"><?php echo $row['price_fee']; ?></td>
    <td class="td06"><?php echo $row['price_deposit']; ?></td>
    <td class="td07"><?php echo $row['into_room']; ?></td>
    <td class="td08"><a href="./details/rental/<?php echo $row['id']; ?>"><img src="../images/availability/btn_detail.jpg" alt="詳細情報" width="82" height="26" /></a></td>
  </tr>
</table>
<?php endforeach; ?>

<p class="c_txt">※空き待ち予約可能。お問い合わせ下さい。<br />
※仲介手数料無料（直接申込みの方）
</p>

</div>
<!-- auxiliary -->

<p class="page_top"><a href="#logo"><img src="../images/common/btn_page-top.jpg" alt="このページのTOPへ" width="90" height="19" /></a></p>
</div>
<!-- sub_main -->

<div id="sub_right">
<div id="side_menu">
  <ul>
    <li><a href="../ci/auxiliary" onmouseover="change_image('s_Menu01',132)" onmouseout="change_image('s_Menu01',131)">
	<img src="../images/common/side_menu/side_menu01.jpg" alt="特優賃・高優賃" width="204" height="35" id="s_Menu01" name="s_Menu01" /></a></li>
	<li><a href="../service/" onmouseover="change_image('s_Menu02',134)" onmouseout="change_image('s_Menu02',133)">
	<img src="../images/common/side_menu/side_menu02.jpg" alt="ミサワハウス特典・サービス" width="204" height="35" id="s_Menu02" name="s_Menu02" /></a></li>
	<li><a href="../ci/monthly" onmouseover="change_image('s_Menu03',136)" onmouseout="change_image('s_Menu03',135)">
	<img src="../images/common/side_menu/side_menu03.jpg" alt="デイリー・ウィークリー・マンスリー" width="204" height="35" id="s_Menu03" name="s_Menu03" /></a></li>
	<li><a href="../ci/dormitory" onmouseover="change_image('s_Menu04',138)" onmouseout="change_image('s_Menu04',137)">
	<img src="../images/common/side_menu/side_menu04.jpg" alt="会社寮" width="204" height="35" id="s_Menu04" name="s_Menu04" /></a></li>
	<li><a href="../apartment_house/" onmouseover="change_image('s_Menu05',140)" onmouseout="change_image('s_Menu05',139)">
	<img src="../images/common/side_menu/side_menu05.jpg" alt="自社マンション一覧" width="204" height="35" id="s_Menu05" name="s_Menu05" /></a></li>
	<li><a href="../" onmouseover="change_image('s_Menu06',142)" onmouseout="change_image('s_Menu06',141)">
	<img src="../images/common/side_menu/side_menu06.jpg" alt="ミサワハウスの日進月歩帳" width="204" height="35" id="s_Menu06" name="s_Menu06" /></a></li>	
  </ul>
</div>
<!-- side_menu -->

<p class="bnr"><a href="../ci/availability" onmouseover="change_image('s_Bnr',154)" onmouseout="change_image('s_Bnr',153)">
<img src="../images/common/bnr_info_s.jpg" alt="マンション空室情報" width="204" height="84" id="s_Bnr" name="s_Bnr" /></a></p>

<p class="bnr"><a href="../subdivision/" onmouseover="change_image('s_Bnr02',156)" onmouseout="change_image('s_Bnr02',155)">
<img src="../images/common/bnr_subdivision_s.jpg" alt="自社分譲地情報" width="204" height="84" id="s_Bnr02" name="s_Bnr02" /></a></p>

<p class="bnr"><img src="../images/common/bnr_qr_s.jpg" alt="ミサワハウスの携帯サイト" width="204" height="130" /></p>



</div>
<!-- sub_right -->

<div class="clr_sps"></div>

</div>
<!-- contents -->
</div>
<!-- container -->
<div id="foot_bg">
<div id="foot">
<p class="foot_menu">
<a href="../">HOME</a>｜
<a href="../ci/auxiliary">特優賃・高優賃</a>｜
<a href="../service/">ミサワハウス特典・サービス</a>｜
<a href="../ci/monthly">デイリー・ウィークリー・マンスリー</a>｜
<a href="../ci/dormitory">会社寮</a>｜
<a href="../apartment_house/">自社マンション一覧</a>｜
<a href="../subdivision/">自社分譲地情報</a>
<br />
<a href="../ci/availability">空室情報一覧</a>｜
<a href="../about_us/">会社概要</a>｜
<a href="../request/">パンフレット請求</a>｜
<a href="../contact/">お問い合わせ</a>｜
<a href="../privacy/">プライバシーポリシー</a>｜
<a href="../link/">リンク</a>｜
<a href="../sitemap/">サイトマップ</a>
</p>
<div id="foot_bottom">
<p class="copy">Copyright&copy; 2010 MISAWA HOUSE All Rights Reserved.</p>
<p class="foot_ad"><img src="../images/common/foot_ad.jpg" alt="愛媛県松山市美沢2丁目5-62　TEL：089-925-7632　株式会社ミサワハウス" width="313" height="72" /></p>
<p class="clr_sps"></p>
</div>
</div>
<!-- foot -->
</div>
<!-- foot_bg -->
</div>
<!-- bg -->

</body>
</html>
