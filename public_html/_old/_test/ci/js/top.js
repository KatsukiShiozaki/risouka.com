var topPage = function() {
  var that = {
    rows: [],
    getRows: function() {
      jQuery.ajax({
        url: './ci/information/get/',
        type: 'POST',
        dataType: 'json',
        success: function(json) {
          if (json.result === 'okay') {
            that.rows         = json.rows;
            that.setRows();
          }
        }
      });
    },
    setRows: function() {
      var contents = '';
      jQuery.each(that.rows, function(i, row) {
        var date = row.date.replace(/-/g, '/');
        contents += ''
        + '<p class="n_day">' + date + '</p>'
        + '<p class="n_txt"><a href="./information/?news_' + row.id + '">' + row.title + '</a></p>'
        ;
      });
      jQuery('#news').html(contents);
    }
  };
  return that;
};

var topObj = new topPage();
jQuery(document).ready(function() {
  topObj.getRows();
});
