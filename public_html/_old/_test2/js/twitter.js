// JavaScript Document

function relative_time(time_value) {
  time_values = time_value.split(" ");
  time_value = time_values[1]+" "+time_values[2]+", "+time_values[5]+" "+time_values[3];
  var parsed_date = Date.parse(time_value);
  var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
  var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
  delta = delta + (relative_to.getTimezoneOffset()*60);
  var dt = new Date();
  dt.setTime(dt.getTime() - (delta*1000));
  yy = dt.getYear();
  mm = dt.getMonth() + 1;
  dd = dt.getDate();
  dy = dt.getDay();
  hh = dt.getHours();
  mi = dt.getMinutes();
  ss = dt.getSeconds();
  if (yy < 2000) { yy += 1900; }
  if (mm < 10) { mm = "0" + mm; }
  if (dd < 10) { dd = "0" + dd; }
  dy = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sst")[dy];
  if (hh < 10) { hh = "0" + hh; }
  if (mi < 10) { mi = "0" + mi; }
  if (ss < 10) { ss = "0" + ss; }

  return yy+"/"+mm+"/"+dd+"("+dy+") "+hh+":"+mi+":"+ss;

}
function twitterCallback(obj) {
  var t = document.getElementById('twitter');
  for ( i=0; i<obj.length; i++) {
    t.innerHTML += '<ul>';
    for ( i=0; i<obj.length; i++) {
        t.innerHTML += '<li>'+obj[i].text+'<br /><span class="time">'+relative_time(obj[i].created_at)+'</span></li>';
    }
    t.innerHTML += '</ul>';
    t_mdl = document.createElement("div");
    t_mdl.setAttribute("class","twitter_middle");
    t_time = document.createElement("span");
    t_time.setAttribute("class","twitter_time");
    t_text = document.createElement("span");
    t_text.setAttribute("class","twitter_text");
    t_mdl.appendChild(t_time);
    t_mdl.appendChild(t_text);
    t.appendChild(t_mdl);
  }
}
document.write(
 '<scr'+'ipt '
+'type="text/javascript" '
+'src="http://www.twitter.com/statuses/user_timeline/risouka2010.json?callback=twitterCallback&count=3">'
+'</scr'+'ipt>'
);
