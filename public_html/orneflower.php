<?php include_once("include/config.php"); ?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>オルネフラワー｜<?= $site_name ?></title>
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta http-equiv="content-script-type" content="text/javascript">
<meta http-equiv="content-style-type" content="text/css">
<link href="css/import.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<script type="text/javascript" src="./ci/js/top.js"></script>
	<!--colorbox-->
<link href="css/colorbox.css" rel="stylesheet" type="text/css" charset="UTF-8" />
<script src="js/jquery.colorbox.js"></script>
<script type="text/javascript">
$(function() {
    $('.colorbox a').colorbox({ rel: 'group' });
});
</script> 



</head>
<body class="home">




<div id="bg">
<div id="bg_sub">
<div id="container">



<header>
<?php require("tpl_header.php"); ?>
</header>
    





<article id="content">

<div id="main">
<div id="dryflower">
    
 
<h2><img src="images/title-orne.png" alt="オルネフラワー"></h2>

<section>

<p class="mb10"><img src="images/orne/catch.jpg" alt="いつまでも失わない輝きを・・・"></p>

<div class="txt txt_catch">
	<!--<p>数種類のワイヤーの特徴を生かし、お花の一枚一枚を丁寧に作り上げていきます。</p>
	<p>また、ワイヤーは柔軟であるため、お花に様々な表情を生み出すことが出来ます。</p>
	<p>散ることなく輝きを持つ綺麗なお花を、あなたのお気に入りの場所に飾りつけましょう。</p>-->
	<p>金属装飾をあしらった斬新なデザインは見る方を魅了します。</p><p>ワイヤーの特徴に合わせてお花を丁寧に作っていきます。</p><p>表情の違う数種類のワイヤーの組み合わせで柔軟な唯一の輝きをもつお花が完成します。</p><p>額、ボトル、アレンジ、花束などと合わせてたりアクセサリー制作も可能です。 </p>
</div>
 
</section>
	
	<section>
<h3>製作ギャラリー<span>　用途に合わせたオルネフラワーの製作事例です。 </span></h3>

    <div class="txt">
      <p>オルネフラワーは、精密な作りをしており、その姿は生花と変わらず生き生きとしております。<br>
		  また丈夫なワイヤーなので、綺麗な形を保つことが出来ます。
		</p>
    </div>
 
<div class="gallery_box colorbox">
    
    <ul>
    
        <li><a href="images/orne/1.jpg"><img src="images/orne/1_thum.jpg" alt="製作事例01" ></a></li>
        <li><a href="images/orne/2.jpg"><img src="images/orne/2_thum.jpg" alt="製作事例02" ></a></li>
        <li><a href="images/orne/3.jpg"><img src="images/orne/3_thum.jpg" alt="製作事例03" ></a></li>
        <li><a href="images/orne/4.jpg"><img src="images/orne/4_thum.jpg" alt="製作事例04" ></a></li>
        
        <li><a href="images/orne/5.jpg"><img src="images/orne/5_thum.jpg" alt="製作事例05" ></a></li>
        <li><a href="images/orne/6.jpg"><img src="images/orne/6_thum.jpg" alt="製作事例06" ></a></li>
        <li><a href="images/orne/7.jpg"><img src="images/orne/7_thum.jpg" alt="製作事例07" ></a></li>
        <li><a href="images/orne/8.jpg"><img src="images/orne/8_thum.jpg" alt="製作事例08" ></a></li>
    
    </ul>
    


</div>
<!-- gallery_box -->    

</section>

<!--<section>

<h3>クリスタルデコラシオンフルール<span>（クリスタル立体）</span></h3>


<div class="txt catch_sub">
<p>クリスタルに包まれてまるでブーケのように・・・。<br>
  どの角度からもお花の色どりや形を楽しむことができます。</p>
</div>

<p class="center"><img src="images/dryflower/pic01.jpg" alt="" /></p>

</section>

-->

<!--
<section>

<h3>クデコラシオンフルール<span>（立体額）</span></h3>


<div class="txt catch_sub">
<p>額に入れて生け花ブーケのイメージを活かしたアレンジに・・・。<br>
台紙やフレームで様々なイメージに変わります。</p>
</div>

<p class="center"><img src="images/dryflower/pic02.jpg" alt="" /></p>

</section>
-->



<!--<section>

<h3>プレセフルール<span>（押花額）</span></h3>


<div class="txt catch_sub">
<p>まるで絵画のようにフレームにおさまった花びらのアート。思い出を重ねるように、花びらも一枚一枚丁寧に重ねていきます。</p>
</div>

<p class="center"><img src="images/dryflower/pic03.jpg" alt="" /></p>

</section>-->









 
<!--<p class="line1"><img src="images/common/line1-btm.png" alt=""></p>-->










</div><!-- dryflower -->
</div><!-- main -->

    

</article><!-- content -->




<footer>
<?php require("tpl_footer.php"); ?>
</footer>

</div><!-- container -->
</div><!-- bg_sub -->
</div><!-- bg -->



</body>
</html>